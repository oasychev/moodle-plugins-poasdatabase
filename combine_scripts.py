# combine_scripts.py

import re
import os.path

# configuration

src_folder = '.'  # where to read from
dst_folder = '.'  # where to write to

script_names = [
	'1_Users.txt',
	'2_Courses.txt',
	'3_Enrolments.txt',
	'4_marks.txt',
]

line_comment_mark = r'(?:#|-- |/\*[\S\s]*?\*/)'

combined_script_name = 'Scripts_1-4_%s.sql'

version_string = None  # use current date
# version_string = '2023-11-13'  # use specific date as version


# code

MUTLIPLE_NL_re = re.compile(r'\n{3,}')

def remove_extra_empty_lines(text):
    return MUTLIPLE_NL_re.sub(r'\n\n', text)

LINE_COMMENT_re = re.compile(r'^\s*%s.*(?:\n|$)' % line_comment_mark, re.M)

def remove_comments(text):
    if line_comment_mark:
        return LINE_COMMENT_re.sub('', text)
    return text

def file_content(path, basedir=None):
    try:
        with open(path) as f:
            text = f.read()
            text = text.strip()
            text = remove_comments(text)
            text = remove_extra_empty_lines(text)
            text = text.strip()
            return text
    except Exception as e:
        if basedir:
            return file_content(os.path.join(basedir, path))
        print(type(e), e)
        print(f'>>>>>>>>ERROR reading file: "{path}"')


def current_date():
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d')
    return formatted_time


def main():
	date_string = current_date()
	version = version_string or date_string
	full_text = ''

	for file_name in script_names:
		full_text += f'##### {file_name} #####\n\n'
		full_text += file_content(file_name, src_folder)
		full_text += '\n\n'

	full_text += f'# ------ Version: {version}\n'
	full_text += f'# ----- Saved at: {date_string}\n'

	combined_script_path = os.path.join(dst_folder, combined_script_name % version)
	with open(combined_script_path, 'w') as f:
	    f.write(full_text)
	print('saved:', combined_script_path)


if __name__ == '__main__':
    main()

