<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Database enrolment plugin.
 *
 * This plugin synchronises enrolment and roles with external database table.
 *
 * @package    enrol_poasdatabase
 * @copyright  2023 Oleg Sychev
 * @copyright  based on work by 2010 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * POAS database enrolment plugin implementation — customized database enrolment plugin.
 * @copyright 2023 onwards Oleg Sychev
 * @author    based on work by Petr Skoda - based on code by Martin Dougiamas, Martin Langhoff and others
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class enrol_poasdatabase_plugin extends enrol_plugin {

    /**
     * Is it possible to delete enrol instance via standard UI?
     *
     * @param stdClass $instance
     * @return bool
     */
    public function can_delete_instance($instance): bool {
        $context = context_course::instance($instance->courseid);
        if (!has_capability('enrol/database:config', $context)) {
            return false;
        }
        if (!enrol_is_enabled('poasdatabase')) {
            return true;
        }
        if (!$this->get_config('dbtype')
            || !$this->get_config('remoteenroltable')
            || !$this->get_config('remotecoursefield')
            || !$this->get_config('remoteuserfield')) {
            return true;
        }

        // TODO: connect to external system and make sure no users are to be enrolled in this course.
        return false;
    }

    /**
     * Is it possible to hide/show enrol instance via standard UI?
     *
     * @param stdClass $instance
     * @return bool
     */
    public function can_hide_show_instance($instance) {
        $context = context_course::instance($instance->courseid);
        return has_capability('enrol/database:config', $context);
    }

    /**
     * Does this plugin allow manual unenrolment of a specific user?
     * Yes, but only if user suspended...
     *
     * @param stdClass $instance course enrol instance
     * @param stdClass $ue record from user_enrolments table
     *
     * @return bool - true means user with 'enrol/xxx:unenrol' may unenrol this user,
     *   false means nobody may touch this user enrolment
     */
    public function allow_unenrol_user(stdClass $instance, stdClass $ue) {
        if ($ue->status == ENROL_USER_SUSPENDED) {
            return true;
        }

        return false;
    }

    /**
     * Forces synchronisation of user enrolments with external database,
     * does not create new courses.
     *
     * This function is called right after every user login.
     *
     * @param stdClass $user user record
     * @return void
     * @throws coding_exception
     * @throws dml_exception
     */
    public function sync_user_enrolments($user) {
        global $CFG, $DB;

        // We do not create courses here intentionally because it requires full sync and is slow.
        if (!$this->get_config('dbtype')
            || !$this->get_config('remoteenroltable')
            || !$this->get_config('remotecoursefield')
            || !$this->get_config('remoteuserfield')) {
            return;
        }

        $table = $this->get_config('remoteenroltable');
        $coursefield = trim($this->get_config('remotecoursefield'));
        $userfield = trim($this->get_config('remoteuserfield'));
        $rolefield = trim($this->get_config('remoterolefield'));
        $otheruserfield = trim($this->get_config('remoteotheruserfield'));

        // Lowercased versions - necessary because we normalise the resultset with array_change_key_case().
        $coursefieldl = strtolower($coursefield);
        $userfieldl = strtolower($userfield);
        $rolefieldl = strtolower($rolefield);
        $otheruserfieldlower = strtolower($otheruserfield);

        $localrolefield = $this->get_config('localrolefield');
        $localuserfield = $this->get_config('localuserfield');
        $localcoursefield = $this->get_config('localcoursefield');

        $unenrolaction = $this->get_config('unenrolaction');
        $defaultrole = $this->get_config('defaultrole');

        $ignorehidden = $this->get_config('ignorehiddencourses');

        $allowreenablingteachers = $this->get_config('allow_reenabling_teachers');

        if (!is_object($user) || !property_exists($user, 'id')) {
            throw new coding_exception('Invalid $user parameter in sync_user_enrolments()');
        }

        if (!property_exists($user, $localuserfield)) {
            debugging('Invalid $user parameter in sync_user_enrolments(), missing ' . $localuserfield);
            $user = $DB->get_record('user', array('id' => $user->id));
        }

        // Create roles mapping.
        $allroles = get_all_roles();
        if (!isset($allroles[$defaultrole])) {
            $defaultrole = 0;
        }
        $roles = array();
        foreach ($allroles as $role) {
            $roles[$role->$localrolefield] = $role->id;
        }
        $editingteacherroleid = $roles['editingteacher'];

        $roleassigns = array();
        $enrols = array();
        $instances = array();

        if (!$extdb = $this->db_init()) {
            // Can not connect to database, sorry.
            return;
        }

        // Read remote enrols and create instances.
        $sql = $this->db_get_sql($table, array($userfield => $user->$localuserfield), array(), false);

        if ($rs = $extdb->Execute($sql)) {
            if (!$rs->EOF) {
                while ($fields = $rs->FetchRow()) {
                    $fields = array_change_key_case($fields, CASE_LOWER);
                    $fields = $this->db_decode($fields);

                    if (empty($fields[$coursefieldl])) {
                        // Missing course info.
                        continue;
                    }
                    if (!$course = $DB->get_record('course', array($localcoursefield => $fields[$coursefieldl]), 'id,visible')) {
                        continue;
                    }
                    if (!$course->visible && $ignorehidden) {
                        continue;
                    }

                    if (empty($fields[$rolefieldl]) || !isset($roles[$fields[$rolefieldl]])) {
                        if (!$defaultrole) {
                            // Role is mandatory.
                            continue;
                        }
                        $roleid = $defaultrole;
                    } else {
                        $roleid = $roles[$fields[$rolefieldl]];
                    }

                    $roleassigns[$course->id][$roleid] = $roleid;
                    if (empty($fields[$otheruserfieldlower])) {
                        $enrols[$course->id][$roleid] = $roleid;
                    }

                    if ($instance = $DB->get_record('enrol',
                            array(
                                'courseid' => $course->id,
                                'enrol' => 'poasdatabase'
                            ), '*', IGNORE_MULTIPLE)) {
                        $instances[$course->id] = $instance;
                        continue;
                    }

                    $enrolid = $this->add_instance($course);
                    $instances[$course->id] = $DB->get_record('enrol', array('id' => $enrolid));
                }
            }
            $rs->Close();
            $extdb->Close();
        } else {
            // Bad luck, something is wrong with the db connection.
            $extdb->Close();
            return;
        }

        // Enrol user into courses and sync roles.
        foreach ($roleassigns as $courseid => $roles) {
            if (!isset($instances[$courseid])) {
                // Ignored.
                continue;
            }
            $instance = $instances[$courseid];

            if (isset($enrols[$courseid])) {
                if ($e = $DB->get_record('user_enrolments', array('userid' => $user->id, 'enrolid' => $instance->id))) {
                    // Reenable enrolment when previously disable enrolment refreshed.
                    if ($e->status == ENROL_USER_SUSPENDED) {
                        $this->update_user_enrol($instance, $user->id, ENROL_USER_ACTIVE);
                    }
                } else {
                    $roleid = reset($enrols[$courseid]);
                    $this->enrol_user($instance, $user->id, $roleid, 0, 0, ENROL_USER_ACTIVE);
                }
            }

            if (!$context = context_course::instance($instance->courseid, IGNORE_MISSING)) {
                // Weird.
                continue;
            }
            $current = $DB->get_records('role_assignments', array(
                    'contextid' => $context->id,
                    'userid' => $user->id,
                    'component' => 'enrol_poasdatabase',
                    'itemid' => $instance->id),
                '', 'id, roleid');

            $existing = array();
            foreach ($current as $r) {
                if (isset($roles[$r->roleid])) {
                    $existing[$r->roleid] = $r->roleid;
                } else {
                    role_unassign($r->roleid, $user->id, $context->id, 'enrol_poasdatabase', $instance->id);
                }
            }
            foreach ($roles as $rid) {
                if (!isset($existing[$rid])) {
                    role_assign($rid, $user->id, $context->id, 'enrol_poasdatabase', $instance->id);
                }
            }
        }

        // Unenrol as necessary.
        $sql = "SELECT e.*, c.visible AS cvisible, ue.status AS ustatus, ra.roleid as roleid
                  FROM {enrol} e
                  JOIN {course} c ON c.id = e.courseid
                  JOIN {role_assignments} ra ON ra.itemid = e.id
             LEFT JOIN {user_enrolments} ue ON ue.enrolid = e.id AND ue.userid = ra.userid
                 WHERE ra.userid = :userid AND e.enrol = 'poasdatabase'";
        $rs = $DB->get_recordset_sql($sql, array('userid' => $user->id));
        foreach ($rs as $instance) {
            if (!$instance->cvisible && $ignorehidden) {
                continue;
            }

            if (!$context = context_course::instance($instance->courseid, IGNORE_MISSING)) {
                // Very weird.
                continue;
            }

            // If the user is still in external database ...
            // TODO: handle the case if they not.
            if (!empty($enrols[$instance->courseid])) {
                // We want this user enrolled.
                continue;
            }

            // Note: >> hardcoded handling of specific role >>
            // activate lecturers anyway ...
            if ($allowreenablingteachers && $instance->roleid == $editingteacherroleid) {
                // Double check: keep lecturers enrolled.
                if ($instance->ustatus == ENROL_USER_SUSPENDED) {
                    // Activate the enrolment.
                    $this->update_user_enrol($instance, $user->id, ENROL_USER_ACTIVE);
                }
                continue;
            }
            // Note: ^^ hardcoded handling of specific role ^^ .

            // Deal with enrolments removed from external table ...
            if ($unenrolaction == ENROL_EXT_REMOVED_UNENROL) {
                $this->unenrol_user($instance, $user->id);

            } else if ($unenrolaction == ENROL_EXT_REMOVED_KEEP) {
                ; // Keep - only adding enrolments.

            } else if ($unenrolaction == ENROL_EXT_REMOVED_SUSPEND || $unenrolaction == ENROL_EXT_REMOVED_SUSPENDNOROLES) {
                // Suspend users.
                if ($instance->ustatus != ENROL_USER_SUSPENDED) {
                    $this->update_user_enrol($instance, $user->id, ENROL_USER_SUSPENDED);
                }
                if ($unenrolaction == ENROL_EXT_REMOVED_SUSPENDNOROLES) {
                    if (!empty($roleassigns[$instance->courseid])) {
                        // We want this "other user" to keep their roles.
                        continue;
                    }
                    role_unassign_all(array(
                        'contextid' => $context->id,
                        'userid' => $user->id,
                        'component' => 'enrol_poasdatabase',
                        'itemid' => $instance->id));
                }
            }
        }
        $rs->close();
    }

    /**
     * Adds lagging students into enrolments table (remoteenroltable) using searching for a course by exact of approximate idnumber.
     * Sets $this->lagging_stats variable to array with int statistcs on how and how many students were processed
     *
     * @param progress_trace $trace
     * @param ADONewConnection $extdb object
     * @return int 0 means success, 1 db connect failure, 2 db read failure
     * @throws dml_exception
     */
    public function prepare_lagging_students(progress_trace $trace, $extdb): int {

        global $DB;

        // TODO: check settings & exit when wrong.

        // Default field value when not OFF (not empty): lagging_students.
        $tablelagging = trim($this->get_config('laggingstudentstable'));

        if (empty($tablelagging)) {
            $trace->output("Skipping lagging students sync.", 1);
            return 0;
        }

        // Default field values: student_login, exact_course_idnumber, course_idnumber_pattern.
        $colstudentlogin = trim($this->get_config('laggingstudentlogin'));
        $colexactcourseidnumber = trim($this->get_config('laggingexactcourseidnumber'));
        $colcourseidnumberpattern = trim($this->get_config('laggingcourseidnumberpattern'));

        if (empty($colstudentlogin) || (empty($colexactcourseidnumber) && empty($colcourseidnumberpattern))) {
            $trace->output("Skipping lagging students sync: the config does not provide sufficient table fields.", 1);
            return 0;
        }

        $table = $this->get_config('remoteenroltable');  // Ex. 'courses_members'.
        $coursefield = trim($this->get_config('remotecoursefield'));  // Ex. 'course_idnumber'.
        $userfield = trim($this->get_config('remoteuserfield'));  // Ex. 'member_login'.
        $rolefield = trim($this->get_config('remoterolefield'));  // Ex. 'member_role'.

        $trace->output("Megring lagging students to general enrolments table ($table) ...");

        // Iterate over external 'lagging_students' table...

        $sql = $this->db_get_sql($tablelagging, /*conditions=*/ array(), /*fields=*/ [], /*distinct=*/ false);

        $courseidnumber2mapping = array();
        $courseidnumbertemplate2mapping = array();

        $emptylogins = 0;

        // Read rows and group by course's idnumber and/or idnumber template ...

        if ($rs = $extdb->Execute($sql)) {
            if (!$rs->EOF) {

                $trace->output("Reading remote lagging students ...", 1);

                while ($mapping = $rs->FetchRow()) {
                    $mapping = $this->db_decode($mapping);

                    if (empty($mapping)) {
                        continue; // Invalid mapping.
                    }

                    $login = $mapping[$colstudentlogin];

                    if (!$login) {
                        // Empty login encountered, skipping.
                        $emptylogins += 1;
                        continue; // Invalid mapping.
                    }

                    if ($mapping[$colexactcourseidnumber]) {
                        $idnumber = $mapping[$colexactcourseidnumber];
                        $courseidnumber2mapping[$idnumber][$login] = $mapping;
                    }
                    if ($mapping[$colcourseidnumberpattern]) {
                        $template = $mapping[$colcourseidnumberpattern];
                        $courseidnumbertemplate2mapping[$template][$login] = $mapping;
                    }
                }
            }
            $rs->Close();
        } else {
            $trace->output('Error reading data from the external enrolment table: $table');
            $extdb->Close();
            return 2;
        }

        $trace->output("Found $emptylogins empty logins in lagging students' table.", 1);
        $trace->output('Found ' . count($courseidnumber2mapping) . ' course idnumbers.', 1);
        $trace->output('Found ' . count($courseidnumbertemplate2mapping) . ' course idnumber templates.', 1);

        // Check if provided course idnumber(s) are valid ...

        $coursesdoesntexist = 0;

        foreach ($courseidnumber2mapping as $exactcourseidnumber => $courserecords) {

            if (!$exactcourseidnumber) {
                continue;
            }

            if (!$DB->record_exists('course', array('idnumber' => $exactcourseidnumber))) {
                // No exact course exist in Moodle.

                $courseidnumber2mapping[$exactcourseidnumber] = null;  // Mark this idnumber 'invalid'.
                $coursesdoesntexist += 1;
                continue;
            }

            foreach ($courserecords as $studentlogin => $mapping) {

                // Skip this mapping during further approximate matching.
                $template = $mapping[$colcourseidnumberpattern];
                if ($template) {
                    // Remove the student entry from templates array since they already have exact course.
                    unset($courseidnumbertemplate2mapping[$template][$studentlogin]);
                }
            }
        }

        $trace->output("Exact courses not found: $coursesdoesntexist", 1);
        $coursesexistapprox = 0;

        // Find courses by approximate pattern that are possibly suitable ...

        foreach ($courseidnumbertemplate2mapping as $courseidnumberpattern => $courserecords) {

            if (!$courseidnumberpattern || empty($courserecords)) {
                continue;
            }

            $courses2enrolon = [];

            $records = $DB->get_records_sql('SELECT id,idnumber,shortname
                FROM {course}
                WHERE ' . $DB->sql_like('idnumber', ':idnum'),
              ['idnum' => $courseidnumberpattern]);

            foreach ($records as $course) {
                $idnumber = $course->idnumber;

                if (array_key_exists($idnumber, $courseidnumber2mapping) && !$courseidnumber2mapping[$idnumber]) {
                    // This course does not exist anyway.
                    continue;
                }

                // Add all "patterned" students to exact course entry.
                foreach ($courserecords as $studentlogin => $mapping) {
                    $courseidnumber2mapping[$idnumber][$studentlogin] = $mapping;
                }

                $coursesexistapprox += 1;
            }
        }

        $trace->output("Approximately suitable courses found: $coursesexistapprox", 1);

        // Make enrolments ...

        $trace->output("Inserting enrolment rows to external database ...", 1);
        $enrolmentrows = 0;
        $studentrole = 'student';

        foreach ($courseidnumber2mapping as $exactcourseidnumber => $courserecords) {
            if (!$exactcourseidnumber) {
                continue;
            }

            foreach ($courserecords as $studentlogin => $mapping) {
                $sql = (
                "INSERT INTO $table ($coursefield, $userfield, member_role)
                SELECT * FROM (
                    SELECT '$exactcourseidnumber' AS $coursefield, '$studentlogin' AS $userfield, '$studentrole' AS memberrole
                    ) AS tmp
                WHERE NOT EXISTS (
                    SELECT * FROM $table t2
                       WHERE t2.$coursefield = '$exactcourseidnumber'
                       AND  t2.$userfield = '$studentlogin'
                       AND  t2.$rolefield = '$studentrole'
                );");

                $enrolmentrows += 1;

                // Setting to enrol: ($student_role) $student_login -->> to course: $exact_course_idnumber  ($course->shortname).

                if ($rs2 = $extdb->Execute($sql)) {
                    $rs2->Close();
                } else {
                    $trace->output("Error read/insert data to the external enrolment table ('$table'). "
                        . 'Please ensure that the external DB user has INSERT capabitities  !!');
                    $extdb->Close();
                    return 2;
                }
            }
        }

        $trace->output("Insertions possibly made (at most): $enrolmentrows", 1);
        $trace->output("Merging lagging students to general enrolments table COMPLETED.");

        return 0;
    }


    /**
     * Forces synchronisation of all enrolments with external database.
     *
     * @param progress_trace $trace
     * @param null|int $onecourse limit sync to one course only (used primarily in restore)
     * @return int 0 means success, 1 db connect failure, 2 db read failure
     * @throws coding_exception
     * @throws dml_exception
     */
    public function sync_enrolments(progress_trace $trace, $onecourse = null) {
        global $CFG, $DB;

        // We do not create courses here intentionally because it requires full sync and is slow.
        if (!$this->get_config('dbtype')
            || !$this->get_config('remoteenroltable')
            || !$this->get_config('remotecoursefield')
            || !$this->get_config('remoteuserfield')) {
            $trace->output('User enrolment synchronisation skipped.');
            $trace->finished();
            return 0;
        }

        $trace->output('Starting user enrolment synchronisation...');

        if (!$extdb = $this->db_init()) {
            $trace->output('Error while communicating with external enrolment database');
            $trace->finished();
            return 1;
        }

        // We may need a lot of memory here.
        core_php_time_limit::raise();
        raise_memory_limit(MEMORY_HUGE);

        $table = $this->get_config('remoteenroltable');
        $coursefield = trim($this->get_config('remotecoursefield'));
        $userfield = trim($this->get_config('remoteuserfield'));
        $rolefield = trim($this->get_config('remoterolefield'));
        $otheruserfield = trim($this->get_config('remoteotheruserfield'));

        // Lowercased versions - necessary because we normalise the resultset with array_change_key_case().
        $coursefieldl = strtolower($coursefield);
        $userfieldl = strtolower($userfield);
        $rolefieldl = strtolower($rolefield);
        $otheruserfieldlower = strtolower($otheruserfield);

        $localrolefield = $this->get_config('localrolefield');
        $localuserfield = $this->get_config('localuserfield');
        $localcoursefield = $this->get_config('localcoursefield');  // E.g. 'idnumber'.

        $unenrolaction = $this->get_config('unenrolaction');
        $defaultrole = $this->get_config('defaultrole');

        // Preparation task: add students to enrol them later together with teachers.
        $this->prepare_lagging_students($trace, $extdb);

        // Create roles mapping.
        $allroles = get_all_roles();
        if (!isset($allroles[$defaultrole])) {
            $defaultrole = 0;
        }

        // Find roles ids.
        $roles = array();
        foreach ($allroles as $role) {
            $roles[$role->$localrolefield] = $role->id;
        }
        $editingteacherroleid = $roles['editingteacher'];
        $studentroleid = $roles['student'];


        if ($onecourse) {
            $sql = "SELECT c.id, c.visible, c.$localcoursefield AS mapping, c.shortname, e.id AS enrolid
                      FROM {course} c
                 LEFT JOIN {enrol} e ON (e.courseid = c.id AND e.enrol = 'poasdatabase')
                     WHERE c.id = :id";
            if (!$course = $DB->get_record_sql($sql, array('id' => $onecourse))) {
                // Course does not exist, nothing to sync.
                return 0;
            }
            if (empty($course->mapping)) {
                // We can not map to this course, sorry.
                return 0;
            }
            if (empty($course->enrolid)) {
                $course->enrolid = $this->add_instance($course);
            }
            $existingcourses = array($course->mapping => $course);

            // Feel free to unenrol everybody, no safety tricks here.
            $preventfullunenrol = false;
            // Course being restored are always hidden, we have to ignore the setting here.
            $ignorehidden = false;

        } else {
            // Get a list of courses to be synced that are in external table.
            $externalcourses = array();
            $sql = $this->db_get_sql($table, array(), array($coursefield), true);
            if ($rs = $extdb->Execute($sql)) {
                if (!$rs->EOF) {
                    while ($mapping = $rs->FetchRow()) {
                        $mapping = reset($mapping);
                        $mapping = $this->db_decode($mapping);
                        if (empty($mapping)) {
                            // Invalid mapping.
                            continue;
                        }
                        $externalcourses[$mapping] = true;
                    }
                }
                $rs->Close();
            } else {
                $trace->output('Error reading data from the external enrolment table');
                $extdb->Close();
                return 2;
            }
            $preventfullunenrol = empty($externalcourses);
            if ($preventfullunenrol && $unenrolaction == ENROL_EXT_REMOVED_UNENROL) {
                $trace->output('Preventing unenrolment of all current users, ' .
                    'because it might result in major data loss, ' .
                    'there has to be at least one record in external enrol table, sorry.', 1);
            }

            $trace->output('Courses in external database: ' . count($externalcourses), 1);

            // First find all existing courses with enrol instance.
            $existingcourses = array();
            $sql = "SELECT c.id, c.visible, c.$localcoursefield AS mapping, e.id AS enrolid, c.shortname, c.idnumber
                      FROM {course} c
                      JOIN {enrol} e ON (e.courseid = c.id AND e.enrol = 'poasdatabase')";
            $rs = $DB->get_recordset_sql($sql); // Watch out for idnumber duplicates.
            foreach ($rs as $course) {
                if (empty($course->mapping)) {
                    continue;
                }
                $existingcourses[$course->mapping] = $course;
                unset($externalcourses[$course->mapping]);
            }
            $rs->close();

            $trace->output('Local courses (in Moodle): ' . count($existingcourses), 1);

            // Add necessary enrol instances that are not present yet.
            $params = array();
            $localnotempty = "";
            if ($localcoursefield !== 'id') {
                $localnotempty = "AND c.$localcoursefield <> :lcfe";
                $params['lcfe'] = '';
            }
            $sql = "SELECT c.id, c.visible, c.$localcoursefield AS mapping, c.shortname, c.idnumber
                      FROM {course} c
                 LEFT JOIN {enrol} e ON (e.courseid = c.id AND e.enrol = 'poasdatabase')
                     WHERE e.id IS NULL $localnotempty";
            $rs = $DB->get_recordset_sql($sql, $params);
            foreach ($rs as $course) {
                if (empty($course->mapping)) {
                    continue;
                }
                if (!isset($externalcourses[$course->mapping])) {
                    // Course not synced or duplicate.
                    continue;
                }
                $course->enrolid = $this->add_instance($course);
                $existingcourses[$course->mapping] = $course;
                unset($externalcourses[$course->mapping]);
            }
            $rs->close();

            // Print list of missing courses.
            if ($externalcourses) {
                $list = implode(', ', array_keys($externalcourses));
                $trace->output("error: following courses do not exist - $list", 1);
                unset($list);
            }

            // Free memory.
            unset($externalcourses);

            $ignorehidden = $this->get_config('ignorehiddencourses');
        }  // End of else related to if ($onecourse).

        // Sync user enrolments.
        $sqlfields = array($userfield);
        if ($rolefield) {
            $sqlfields[] = $rolefield;
        }
        if ($otheruserfield) {
            $sqlfields[] = $otheruserfield;
        }

        $allowreenablingteachers = $this->get_config('allow_reenabling_teachers');

        // Extract staff_code (course_lecturer_idnumber) as a substring from course idnumber.
        // E.g. '/\A\d+\+\d+(?:\.\d+){2,}-\d{8,}\*\d+\/(\d{8,})\s[ОЗ]{2,4}_[НС]{2,}\Z/' .
        $regexpattern = trim($this->get_config('course_idnumber_regex_capturing_teacher'));

        if (empty($regexpattern)) {
            $allowreenablingteachers = false;
        }

        foreach ($existingcourses as $course) {
            if ($ignorehidden && !$course->visible) {
                continue;
            }
            if (!$instance = $DB->get_record('enrol', array('id' => $course->enrolid))) {
                $trace->output("Warning: no enrol instance found for course, skipping it: $course->shortname ($course->idnumber)",
                    3);
                continue; // Weird!
            }
            $context = context_course::instance($course->id);

            if ($instance->status != ENROL_USER_ACTIVE) {
                $this->update_status($instance, ENROL_USER_ACTIVE);

                $trace->output("Activated suspended enrol instance (id: $instance->id) for course: $course->shortname", 2);
            }

            // Determine if editingteacher is still enrolled.
            $courselectureruserid = null;

            // Extract staff_code (course_lecturer_idnumber) as a substring from course idnumber.
            $courselectureridnumber = null;

            $matchedarray = [];
            if ($allowreenablingteachers && preg_match($regexpattern, $course->idnumber, $matchedarray)) {
                if (isset($matchedarray[1])) {
                    $courselectureridnumber = $matchedarray[1];
                }
            }
            unset($matchedarray);

            // Get current list of enrolled users with their roles.
            $currentroles = array();
            $currentenrols = array();
            $currentstatus = array();
            $usermapping = array();
            // Declare here: list of users that need to be enrolled and their roles.
            $requestedroles = array();
            $requestedenrols = array();
            $sql = "SELECT u.$localuserfield AS mapping, u.id AS userid, ue.status, ra.roleid, u.idnumber as useridnumber
                      FROM {user} u
                      JOIN {role_assignments} ra ON (
                            ra.userid = u.id AND ra.component = 'enrol_poasdatabase' AND ra.itemid = :enrolid)
                 LEFT JOIN {user_enrolments} ue ON (ue.userid = u.id AND ue.enrolid = ra.itemid)
                     WHERE u.deleted = 0";
            $params = array('enrolid' => $instance->id);
            if ($localuserfield === 'username') {
                $sql .= " AND u.mnethostid = :mnethostid";
                $params['mnethostid'] = $CFG->mnet_localhost_id;
            }
            $rs = $DB->get_recordset_sql($sql, $params);
            foreach ($rs as $ue) {
                $currentroles[$ue->userid][$ue->roleid] = $ue->roleid;
                $usermapping[$ue->mapping] = $ue->userid;

                if (isset($ue->status)) {
                    $currentenrols[$ue->userid][$ue->roleid] = $ue->roleid;
                    $currentstatus[$ue->userid] = $ue->status;
                }

                // Keeping teachers enrolled.
                if ($ue->roleid == $editingteacherroleid) {
                    $requestedroles[$ue->userid][$editingteacherroleid] = $editingteacherroleid;
                    $requestedenrols[$ue->userid][$editingteacherroleid] = $editingteacherroleid;
                }
                if ($courselectureridnumber && $ue->useridnumber == $courselectureridnumber) {
                    $courselectureruserid = $ue->userid;
                }
            }
            $rs->close();

            // Get list of users that need to be enrolled and their roles.
            // $requestedroles  = array();  // declared above.
            // $requestedenrols = array();  // declared above.
            $sql = $this->db_get_sql($table, array($coursefield => $course->mapping), $sqlfields);
            if ($rs = $extdb->Execute($sql)) {
                if (!$rs->EOF) {
                    $usersearch = array('deleted' => 0);
                    if ($localuserfield === 'username') {
                        $usersearch['mnethostid'] = $CFG->mnet_localhost_id;
                    }
                    while ($fields = $rs->FetchRow()) {
                        $fields = array_change_key_case($fields, CASE_LOWER);
                        if (empty($fields[$userfieldl])) {
                            $trace->output("error: skipping user without mandatory $localuserfield in course '$course->mapping'",
                                1);
                            continue;
                        }
                        $mapping = $fields[$userfieldl];
                        if (!isset($usermapping[$mapping])) {
                            $usersearch[$localuserfield] = $mapping;
                            if (!$user = $DB->get_record('user', $usersearch, 'id', IGNORE_MULTIPLE)) {
                                $trace->output("error: skipping unknown user: "
                                    . "$localuserfield='$mapping' in course '$course->mapping'", 1);
                                continue;
                            }
                            $usermapping[$mapping] = $user->id;
                            $userid = $user->id;
                        } else {
                            $userid = $usermapping[$mapping];
                        }
                        if (empty($fields[$rolefieldl]) || !isset($roles[$fields[$rolefieldl]])) {
                            if (!$defaultrole) {
                                $trace->output("error: skipping user '$userid' in course '$course->mapping'"
                                . ' - missing course and default role', 1);
                                continue;
                            }
                            $roleid = $defaultrole;
                        } else {
                            $roleid = $roles[$fields[$rolefieldl]];
                        }

                        $requestedroles[$userid][$roleid] = $roleid;
                        if (empty($fields[$otheruserfieldlower])) {
                            $requestedenrols[$userid][$roleid] = $roleid;
                        }
                    }
                }
                $rs->Close();
            } else {
                $trace->output("error: skipping course '$course->mapping' - could not match with external database", 1);
                continue;
            }
            unset($usermapping);

            // Ensure teacher is marked to enrol (keep enrolment active) ...
            if ($courselectureruserid === null) {
                // Teacher was not found in (or previously removed from) Moodle.

                if ($courselectureridnumber) {
                    $user = $DB->get_record('user', array('idnumber' => $courselectureridnumber));
                    if ($user) {
                        $courselectureruserid = $user->id;
                    } else {
                        $trace->output("Warning: No teacher found for user.idnumber = '$courselectureridnumber'");
                    }
                }
                if ($courselectureruserid !== null) {
                    $requestedroles[$courselectureruserid][$editingteacherroleid] = $editingteacherroleid;
                    $requestedenrols[$courselectureruserid][$editingteacherroleid] = $editingteacherroleid;
                } else {
                    $trace->output("Warning: No TEACHER at course : $course->shortname ($course->idnumber)");
                }
            }

            // Enrol all users and sync roles.
            foreach ($requestedenrols as $userid => $userroles) {
                foreach ($userroles as $roleid) {
                    if (empty($currentenrols[$userid])) {
                        $this->enrol_user($instance, $userid, $roleid, 0, 0, ENROL_USER_ACTIVE);
                        $currentroles[$userid][$roleid] = $roleid;
                        $currentenrols[$userid][$roleid] = $roleid;
                        $currentstatus[$userid] = ENROL_USER_ACTIVE;
                        $trace->output("enrolling: $userid ==> $course->shortname as " . $allroles[$roleid]->shortname, 1);
                    }
                }

                // Reenable enrolment when previously disable enrolment refreshed.
                if ($currentstatus[$userid] == ENROL_USER_SUSPENDED) {
                    $this->update_user_enrol($instance, $userid, ENROL_USER_ACTIVE);
                    $trace->output("unsuspending: $userid ==> $course->shortname", 1);
                }
            }

            foreach ($requestedroles as $userid => $userroles) {
                // Assign extra roles.
                foreach ($userroles as $roleid) {
                    if (empty($currentroles[$userid][$roleid])) {
                        role_assign($roleid, $userid, $context->id, 'enrol_poasdatabase', $instance->id);
                        $currentroles[$userid][$roleid] = $roleid;
                        $trace->output("assigning roles: $userid ==> $course->shortname as " . $allroles[$roleid]->shortname, 1);
                    }
                }

                // Unassign removed roles.
                foreach ($currentroles[$userid] as $cr) {
                    if (empty($userroles[$cr])) {
                        role_unassign($cr, $userid, $context->id, 'enrol_poasdatabase', $instance->id);
                        unset($currentroles[$userid][$cr]);
                        $trace->output("unsassigning roles: $userid ==> $course->shortname", 1);
                    }
                }

                unset($currentroles[$userid]);
            }

            foreach ($currentroles as $userid => $userroles) {
                // These are roles that exist only in Moodle, not the external database
                // so make sure the unenrol actions will handle them by setting status.
                $currentstatus += array($userid => ENROL_USER_ACTIVE);  // Mark user as actve if no status had been set before.
            }

            // Deal with enrolments removed from external table.
            if ($unenrolaction == ENROL_EXT_REMOVED_UNENROL) {
                if (!$preventfullunenrol) {
                    // Unenrol.
                    foreach ($currentstatus as $userid => $status) {
                        if (isset($requestedenrols[$userid]) || array_key_exists($editingteacherroleid, $currentroles[$userid])) {
                            // Double check: keep lecturers enrolled.
                            continue;
                        }
                        $this->unenrol_user($instance, $userid);
                        $trace->output("unenrolling: $userid ==> $course->shortname", 1);
                    }
                }

            } else if ($unenrolaction == ENROL_EXT_REMOVED_KEEP) {
                ; // Keep - only adding enrolments.

            } else if ($unenrolaction == ENROL_EXT_REMOVED_SUSPEND || $unenrolaction == ENROL_EXT_REMOVED_SUSPENDNOROLES) {
                // Suspend enrolments.
                foreach ($currentstatus as $userid => $status) {
                    if (isset($requestedenrols[$userid]) || array_key_exists($editingteacherroleid, $currentroles[$userid])) {
                        // Double check: keep lecturers enrolled.
                        continue;
                    }
                    if ($status != ENROL_USER_SUSPENDED) {
                        $this->update_user_enrol($instance, $userid, ENROL_USER_SUSPENDED);
                        $trace->output("suspending: $userid ==> $course->shortname", 1);
                    }
                    if ($unenrolaction == ENROL_EXT_REMOVED_SUSPENDNOROLES) {
                        if (isset($requestedroles[$userid])) {
                            // We want this "other user" to keep their roles.
                            continue;
                        }
                        role_unassign_all(array(
                            'contextid' => $context->id,
                            'userid' => $userid,
                            'component' => 'enrol_poasdatabase',
                            'itemid' => $instance->id));

                        $trace->output("unsassigning all roles: $userid ==> $course->shortname", 1);
                    }
                }
            }
        }  // End of foreach over ($existing_courses as $course).

        unset($regexpattern);

        // Close db connection.
        $extdb->Close();

        $trace->output('...user enrolment synchronisation finished.');
        $trace->finished();

        return 0;
    }


    /**
     * Re-activates suspended teachers,
     * Suspend (unenrol?) teachers that were enrolled wrong
     *
     * @param progress_trace $trace
     */
    public function refresh_teachers(progress_trace $trace) {
        global $DB;

        $allowreenablingteachers = $this->get_config('allow_reenabling_teachers');
        $allowunenrol2ndteachers = $this->get_config('allow_unenrol_2nd_teachers');

        if (!$allowreenablingteachers && !$allowunenrol2ndteachers) {
            return;
        }

        $ignorehidden = $this->get_config('ignorehiddencourses');

        if ($allowreenablingteachers) {
            $trace->output('Re-enabling main teachers on their courses ...');
        }

        // Find role of editingteacher.
        $editingteacherroleid = null;
        $allroles = get_all_roles();
        foreach ($allroles as $role) {
            if ($role->shortname == 'editingteacher') {
                $editingteacherroleid = $role->id;
            }
        }

        // 1) Enrolled but blocked
        // teacher idnumber is the same
        // status - blocked
        // course -> enrolled user -> role = teacher , status = blocked

        $count = 0;
        $coursetomainteacher = array();
        $coursetosecondaryteachers = array();

        // Teachers blocked in their courses ...
        $sql = "SELECT distinct u.id AS userid, ue.status, u.idnumber as useridnumber, c.id as courseid, c.idnumber,
            c.visible, e.id as enrolid, c.shortname as shortname,
            concat(u.lastname, ' ', u.firstname, ' ', u.middlename) as teacher_fio
              FROM {user} u
              JOIN {role_assignments} ra ON (ra.userid = u.id AND ra.component = 'enrol_poasdatabase')
         LEFT JOIN {user_enrolments} ue ON (ue.userid = u.id AND ue.enrolid = ra.itemid)
              JOIN {enrol} e ON (ra.itemid = e.id AND e.enrol = 'poasdatabase')
              JOIN {course} c ON (c.id = e.courseid)
         WHERE u.deleted = 0 AND ra.roleid = $editingteacherroleid
             # and ue.status <> 0  # 1 = BLOCKED! - get all here
             and locate('*', c.idnumber) > 0  # в idnumber курса есть '*' - это учебный курс, не факультет, например";
        $rs = $DB->get_recordset_sql($sql);

        foreach ($rs as $course) {
            if (empty($course->idnumber) || empty($course->enrolid)) {
                $trace->output("Skipping invalid course.", 4);
                continue;
            }
            if ($ignorehidden && !$course->visible) {
                $trace->output("Skipping hidden course: $course->idnumber", 4);
                continue;
            }

            if (!$instance = $DB->get_record('enrol', array('id' => $course->enrolid))) {
                $trace->output("Warning: no enrol instance found for course, skipping it: $course->shortname ($course->idnumber)",
                    3);
                continue; // Weird!
            }

            if ($allowreenablingteachers) {
                if ($instance->status != ENROL_USER_ACTIVE) {
                    $this->update_status($instance, ENROL_USER_ACTIVE);

                    $trace->output("Activated suspended enrol instance (id: $instance->id) for course: $course->shortname", 2);
                }

                // Reenable enrolment.
                if ($course->status != 0) {  // User enrolment status.
                    $this->update_user_enrol($instance, $course->userid, ENROL_USER_ACTIVE);
                    $trace->output("unsuspending teacher: $course->userid ($course->useridnumber - $course->teacher_fio)"
                        . " ==> $course->idnumber ($course->shortname)", 1);
                    $count += 1;
                }
            }

            // Deal with primary & secondary teachers.
            if (strpos($course->idnumber, '/' . ($course->useridnumber) . ' ')) {
                $coursetomainteacher[$course->courseid] = $course->userid;
            } else if ($course->status == 0) {
                // Remember active teachers that are not main (by idnumber inclusion).
                $coursetosecondaryteachers[$course->courseid][$course->userid] = "$course->useridnumber - $course->teacher_fio";
            }
        }

        if ($allowreenablingteachers) {
            $trace->output("Teachers re-enabled: $count.");
        }

        if (!$allowunenrol2ndteachers) {
            // Avoid blocking teachers probably incorrectly enrolled.
            return;
        }

        $count = 0;

        foreach ($coursetosecondaryteachers as $courseid => $teachers) {
            if (array_key_exists($courseid, $coursetomainteacher)) {
                $mainteacher = $coursetomainteacher[$courseid];

                foreach ($teachers as $teacherid => $idnumbername) {
                    if ($mainteacher != $teacherid) {
                        $trace->output("Found secondary teacher: $teacherid ($idnumbername)"
                            . " ==> courseid: $courseid (mainteacher id: $mainteacher)", 1);
                        $count += 1;
                    }
                }
            }
        }
        $trace->output("Teachers' (secondary on their course) enrolments shown (not blocked): $count.");
    }


    /**
     * Performs a full sync with external database.
     *
     * First it creates new courses if necessary, then
     * enrols and unenrols users.
     *
     * @param progress_trace $trace
     * @return int 0 means success, 1 db connect failure, 4 db read failure
     */
    public function sync_courses(progress_trace $trace) {
        global $CFG, $DB;

        // Make sure we sync either enrolments or courses.
        if (!$this->get_config('dbtype')
            || !$this->get_config('newcoursetable')
            || !$this->get_config('newcoursefullname')
            || !$this->get_config('newcourseshortname')) {
            $trace->output('Course synchronisation skipped.');
            $trace->finished();
            return 0;
        }

        $trace->output('Starting course synchronisation...');

        // We may need a lot of memory here.
        core_php_time_limit::raise();
        raise_memory_limit(MEMORY_HUGE);

        if (!$extdb = $this->db_init()) {
            $trace->output('Error while communicating with external enrolment database');
            $trace->finished();
            return 1;
        }

        $table = $this->get_config('newcoursetable');
        $fullname = trim($this->get_config('newcoursefullname'));
        $shortname = trim($this->get_config('newcourseshortname'));
        $idnumber = trim($this->get_config('newcourseidnumber'));
        $category = trim($this->get_config('newcoursecategory'));
        $tagstable = $this->get_config('tagstable');
        $sqlidnumberfield = $this->get_config('sqlidnumberfield');
        $sqltagnamefield = $this->get_config('sqltagnamefield');

        // Lowercased versions - necessary because we normalise the resultset with array_change_key_case().
        $fullnamel = strtolower($fullname);
        $shortnamel = strtolower($shortname);
        $idnumberl = strtolower($idnumber);
        $categoryl = strtolower($category);

        $localcategoryfield = $this->get_config('localcategoryfield', 'id');
        $defaultcategory = $this->get_config('defaultcategory');

        if (!$DB->record_exists('course_categories', array('id' => $defaultcategory))) {
            $trace->output("default course category does not exist!", 1);
            $categories = $DB->get_records('course_categories', array(), 'sortorder', 'id', 0, 1);
            $first = reset($categories);
            $defaultcategory = $first->id;
        }

        $trace->output($this->get_name(), 1);
        $sqlfields = array($fullname, $shortname);
        if ($category) {
            $sqlfields[] = $category;
        }
        if ($idnumber) {
            $sqlfields[] = $idnumber;
        }
        $sql = $this->db_get_sql($table, array(), $sqlfields, true);
        $createcourses = array();
        if ($rs = $extdb->Execute($sql)) {
            if (!$rs->EOF) {
                while ($fields = $rs->FetchRow()) {

                    $fields = array_change_key_case($fields, CASE_LOWER);
                    $fields = $this->db_decode($fields);
                    if (empty($fields[$shortnamel]) || empty($fields[$fullnamel])) {
                        $trace->output('error: invalid external course record, shortname and fullname are mandatory: '
                            . var_export($fields, true), 1);
                        continue;
                    }
                    if ($DB->record_exists('course', array('shortname' => $fields[$shortnamel]))) {
                        // Already exists, update.

                        $curidnumber = $idnumber ? $fields[$idnumberl] : '';
                        if ($curidnumber != $DB->get_record('course', array(
                                'shortname' => $fields[$shortnamel]),
                                'idnumber')->idnumber) {
                            require_once("$CFG->dirroot/course/lib.php");
                            $course = $DB->get_record('course', array('shortname' => $fields[$shortnamel]));
                            $course->idnumber = $idnumber ? $fields[$idnumberl] : '';
                            $course->tags = $this->get_tag_by_idnumber($course->idnumber, $extdb,
                                $tagstable, $sqlidnumberfield, $sqltagnamefield);
                            if ($category) {
                                if (empty($fields[$categoryl])) {
                                    // Empty category means use default.
                                    $course->category = $defaultcategory;
                                } else if ($coursecategory = $DB->get_record('course_categories',
                                        array($localcategoryfield => $fields[$categoryl]),
                                        'id')) {

                                    // Yay, correctly specified category!
                                    $course->category = $coursecategory->id;
                                    unset($coursecategory);
                                } else {
                                    // Bad luck, better not continue because unwanted ppl might
                                    // get access to course in different category.
                                    $trace->output('error: invalid category '
                                        . $localcategoryfield
                                        . ', can not create course (1): '
                                        . $fields[$shortnamel], 1);
                                    continue;
                                }
                            } else {
                                $course->category = $defaultcategory;
                            }

                            $course->id = $DB->get_record('course', array('shortname' => $fields[$shortnamel]), 'id')->id;

                            $trace->output("updating course: $course->id, $course->fullname, "
                                . "$course->shortname, $course->idnumber, $course->category", 1);
                            try {
	                            update_course($course);
                            } catch (dml_exception $e) {
                                // show error message in small
                            	$trace->output('      ERROR in DB: ' . $e->getMessage());
                            }
                            $trace->output('      + + + idnumber updated.');
                        }
                        continue;
                    }

                    if ($DB->record_exists('course', array('idnumber' => $fields[$idnumberl]))) {
                        // Already exists, update.

                        $curshortname = $shortname ? $fields[$shortnamel] : '';
                        if ($curshortname != $DB->get_record(
                                'course',
                                array('idnumber' => $fields[$idnumberl]),
                                'shortname')->shortname) {
                            require_once("$CFG->dirroot/course/lib.php");
                            $course = $DB->get_record('course', array('idnumber' => $fields[$idnumberl]));
                            $course->shortname = $shortname ? $fields[$shortnamel] : '';
                            $course->tags = $this->get_tag_by_idnumber($course->idnumber, $extdb,
                                $tagstable, $sqlidnumberfield, $sqltagnamefield);
                            if ($category) {
                                if (empty($fields[$categoryl])) {
                                    // Empty category means use default.
                                    $course->category = $defaultcategory;
                                } else if ($coursecategory = $DB->get_record(
                                        'course_categories',
                                        array($localcategoryfield => $fields[$categoryl]),
                                        'id')) {

                                    // Yay, correctly specified category!
                                    $course->category = $coursecategory->id;
                                    unset($coursecategory);
                                } else {
                                    // Bad luck, better not continue because unwanted ppl might
                                    // get access to course in different category.
                                    $trace->output('error: invalid category '
                                        . $localcategoryfield
                                        . ', can not create course (2): '
                                        . $fields[$shortnamel], 1);
                                    continue;
                                }
                            } else {
                                $course->category = $defaultcategory;
                            }

                            $course->id = $DB->get_record('course', array('idnumber' => $fields[$idnumberl]), 'id')->id;

                            $trace->output("updating course: $course->id, $course->fullname, "
                                . "$course->shortname, $course->idnumber, $course->category", 1);
                            try {
	                            update_course($course);
                            } catch (dml_exception $e) {
                                // show error message in small
                            	$trace->output('      ERROR in DB: ' . $e->getMessage());
                            }
                            $trace->output('      + + + shortname updated.');
                        }
                        continue;
                    }
                    // Allow empty idnumber but not duplicates.
                    if ($idnumber
                        && $fields[$idnumberl] !== ''
                        && $fields[$idnumberl] !== null
                        && $DB->record_exists('course', array('idnumber' => $fields[$idnumberl]))) {
                        $trace->output('error: duplicate idnumber, can not create course: ' . $fields[$shortnamel]
                            . ' [' . $fields[$idnumberl] . ']', 1);
                        continue;
                    }
                    $course = new stdClass();
                    $course->fullname = $fields[$fullnamel];
                    $course->shortname = $fields[$shortnamel];
                    $course->idnumber = $idnumber ? $fields[$idnumberl] : '';
                    $course->tags = $this->get_tag_by_idnumber($course->idnumber, $extdb,
                        $tagstable, $sqlidnumberfield, $sqltagnamefield);
                    if ($category) {
                        if (empty($fields[$categoryl])) {
                            // Empty category means use default.
                            $course->category = $defaultcategory;
                        } else if ($coursecategory = $DB->get_record('course_categories',
                            array($localcategoryfield => $fields[$categoryl]), 'id')) {

                            // Yay, correctly specified category!
                            $course->category = $coursecategory->id;
                            unset($coursecategory);
                        } else {
                            // Bad luck, better not continue because unwanted ppl might get access to course in different category.
                            $trace->output('error: invalid category ' . $localcategoryfield . ', can not create course (3): '
                                . $fields[$shortnamel], 1);
                            continue;
                        }
                    } else {
                        $course->category = $defaultcategory;
                    }
                    $createcourses[] = $course;
                }
            }
            $rs->Close();
        } else {
            $extdb->Close();
            $trace->output('Error reading data from the external course table');
            $trace->finished();
            return 4;
        }
        if ($createcourses) {
            require_once("$CFG->dirroot/course/lib.php");

            $templatecourse = $this->get_config('templatecourse');

            $template = false;
            if ($templatecourse) {
                if ($template = $DB->get_record('course', array('shortname' => $templatecourse))) {
                    $template = fullclone(course_get_format($template)->get_course());
                    if (!isset($template->numsections)) {
                        $template->numsections = course_get_format($template)->get_last_section_number();
                    }
                    unset($template->id);
                    unset($template->fullname);
                    unset($template->shortname);
                    unset($template->idnumber);
                } else {
                    $trace->output("can not find template for new course!", 1);
                }
            }
            if (!$template) {
                $courseconfig = get_config('moodlecourse');
                $template = new stdClass();
                $template->summary = '';
                $template->summaryformat = FORMAT_HTML;
                $template->format = $courseconfig->format;
                $template->numsections = $courseconfig->numsections;
                $template->newsitems = $courseconfig->newsitems;
                $template->showgrades = $courseconfig->showgrades;
                $template->showreports = $courseconfig->showreports;
                $template->maxbytes = $courseconfig->maxbytes;
                $template->groupmode = $courseconfig->groupmode;
                $template->groupmodeforce = $courseconfig->groupmodeforce;
                $template->visible = $courseconfig->visible;
                $template->lang = $courseconfig->lang;
                $template->enablecompletion = $courseconfig->enablecompletion;
                $template->startdate = usergetmidnight(time());
                if ($courseconfig->courseenddateenabled) {
                    $template->enddate = usergetmidnight(time()) + $courseconfig->courseduration;
                }
            }

            foreach ($createcourses as $fields) {
                $newcourse = clone($template);
                $newcourse->fullname = $fields->fullname;
                $newcourse->shortname = $fields->shortname;
                $newcourse->idnumber = $fields->idnumber;
                $newcourse->category = $fields->category;
                $newcourse->tags = $fields->tags;

                // Detect duplicate data once again, above we can not find duplicates
                // in external data using DB collation rules...
                if ($DB->record_exists('course', array('shortname' => $newcourse->shortname))) {
                    $trace->output("can not insert new course, duplicate shortname detected: " . $newcourse->shortname, 1);
                    continue;
                } else if (!empty($newcourse->idnumber)
                    && $DB->record_exists('course', array('idnumber' => $newcourse->idnumber))) {
                    $trace->output("can not insert new course, duplicate idnumber detected: " . $newcourse->idnumber, 1);
                    continue;
                }
                $c = create_course($newcourse);
                $trace->output("creating course: $c->id, $c->fullname, $c->shortname, $c->idnumber, $c->category", 1);
            }

            unset($createcourses);
            unset($template);
        }

        // Close db connection.
        $extdb->Close();

        $trace->output('...course synchronisation finished.');
        $trace->finished();

        return 0;
    }

    /** Return the text of SQL query consisting of `SELECT ... FROM $table WHERE ...` formatted for provided $conditions and $sort, as well as optional returned $fields and distinction.
     *
     * @param string $table table name
     * @param array $conditions pairs of things to compare with '='
     * @param array|null $fields list of fields to select, or empty list (that means '*')
     * @param bool $distinct whether to add DISTINCT
     * @param string $sort expression for SORT BY
     * @return string
     */
    protected function db_get_sql($table, array $conditions, ?array $fields, bool $distinct = false, string $sort = ""): string
    {
        $fields = $fields ? implode(',', $fields) : "*";
        $where = array();
        if ($conditions) {
            foreach ($conditions as $key => $value) {
                $value = $this->db_encode($this->db_addslashes($value));

                $where[] = "$key = '$value'";
            }
        }
        $where = $where ? "WHERE " . implode(" AND ", $where) : "";
        $sort = $sort ? "ORDER BY $sort" : "";
        $distinct = $distinct ? "DISTINCT" : "";
        $sql = "SELECT $distinct $fields
                  FROM $table
                 $where
                  $sort";

        return $sql;
    }

    /**
     * Tries to make connection to the external database.
     *
     * @return ADONewConnection|false|null
     */
    protected function db_init() {
        global $CFG;

        require_once($CFG->libdir . '/adodb/adodb.inc.php');

        // Connect to the external database (forcing new connection).
        $extdb = ADONewConnection($this->get_config('dbtype'));
        if ($this->get_config('debugdb')) {
            $extdb->debug = true;
            ob_start(); // Start output buffer to allow later use of the page headers.
        }

        // The dbtype my contain the new connection URL, so make sure we are not connected yet.
        if (!$extdb->IsConnected()) {
            $result = $extdb->Connect(
                $this->get_config('dbhost'),
                $this->get_config('dbuser'),
                $this->get_config('dbpass'),
                $this->get_config('dbname'), true);
            if (!$result) {
                return null;
            }
        }

        $extdb->SetFetchMode(ADODB_FETCH_ASSOC);
        if ($this->get_config('dbsetupsql')) {
            $extdb->Execute($this->get_config('dbsetupsql'));
        }
        return $extdb;
    }

    /** Text escaping for DB.
     * @param string $text to escape.
     * @return string
     */
    protected function db_addslashes($text) {
        // Use custom-made function for now - it is better to not rely on adodb or php defaults.
        if ($this->get_config('dbsybasequoting')) {
            $text = str_replace('\\', '\\\\', $text);
            $text = str_replace(array('\'', '"', "\0"), array('\\\'', '\\"', '\\0'), $text);
        } else {
            $text = str_replace("'", "''", $text);
        }
        return $text;
    }

    /** Convert text from utf-8 to DB-specific encoding (as configured in plugin settings).
     * @param string|array $text text to encode
     * @return array|bool|string
     */
    protected function db_encode($text) {
        $dbenc = $this->get_config('dbencoding');
        if (empty($dbenc) || $dbenc == 'utf-8') {
            return $text;
        }
        if (is_array($text)) {
            foreach ($text as $k => $value) {
                $text[$k] = $this->db_encode($value);
            }
            return $text;
        } else {
            return core_text::convert($text, 'utf-8', $dbenc);
        }
    }

    /** Convert text from DB-specific encoding (as configured in plugin settings) to utf-8.
     * @param string|array $text text to decode
     * @return array|bool|string
     */
    protected function db_decode($text) {
        $dbenc = $this->get_config('dbencoding');
        if (empty($dbenc) || $dbenc == 'utf-8') {
            return $text;
        }
        if (is_array($text)) {
            foreach ($text as $k => $value) {
                $text[$k] = $this->db_decode($value);
            }
            return $text;
        } else {
            return core_text::convert($text, $dbenc, 'utf-8');
        }
    }

    /**
     * Automatic enrol sync executed during restore.
     * @param stdClass $course course record
     */
    public function restore_sync_course($course) {
        $trace = new null_progress_trace();
        $this->sync_enrolments($trace, $course->id);
    }

    /**
     * Restore instance and map settings.
     *
     * @param restore_enrolments_structure_step $step
     * @param stdClass $data
     * @param stdClass $course
     * @param int $oldid
     */
    public function restore_instance(restore_enrolments_structure_step $step, stdClass $data, $course, $oldid) {
        global $DB;

        if ($instance = $DB->get_record('enrol', array('courseid' => $course->id, 'enrol' => $this->get_name()))) {
            $instanceid = $instance->id;
        } else {
            $instanceid = $this->add_instance($course);
        }
        $step->set_mapping('enrol', $oldid, $instanceid);
    }

    /**
     * Restore user enrolment.
     *
     * @param restore_enrolments_structure_step $step
     * @param stdClass $data
     * @param stdClass $instance
     * @param int $userid
     * @param int $oldinstancestatus
     * @throws coding_exception
     * @throws dml_exception
     */
    public function restore_user_enrolment(restore_enrolments_structure_step $step, $data, $instance, $userid, $oldinstancestatus) {
        global $DB;

        if ($this->get_config('unenrolaction') == ENROL_EXT_REMOVED_UNENROL) {
            // Enrolments were already synchronised in restore_instance(), we do not want any suspended leftovers.
            return;
        }
        if (!$DB->record_exists('user_enrolments', array('enrolid' => $instance->id, 'userid' => $userid))) {
            $this->enrol_user($instance, $userid, null, 0, 0, ENROL_USER_SUSPENDED);
        }
    }

    /**
     * Restore role assignment.
     *
     * @param stdClass $instance
     * @param int $roleid
     * @param int $userid
     * @param int $contextid
     */
    public function restore_role_assignment($instance, $roleid, $userid, $contextid) {
        if ($this->get_config('unenrolaction') == ENROL_EXT_REMOVED_UNENROL
            || $this->get_config('unenrolaction') == ENROL_EXT_REMOVED_SUSPENDNOROLES) {
            // Role assignments were already synchronised in restore_instance(), we do not want any leftovers.
            return;
        }
        role_assign($roleid, $userid, $contextid, 'enrol_' . $this->get_name(), $instance->id);
    }

    /**
     * Test plugin settings, print info to output.
     */
    public function test_settings() {
        global $CFG, $OUTPUT;

        // NOTE: this is not localised intentionally, admins are supposed to understand English at least a bit...

        raise_memory_limit(MEMORY_HUGE);

        $this->load_config();

        $enroltable = $this->get_config('remoteenroltable');
        $coursetable = $this->get_config('newcoursetable');
        $cohortcoursetable = $this->get_config('groupcohortcoursestable');

        if (empty($enroltable)) {
            echo $OUTPUT->notification('External enrolment table not specified.', 'notifyproblem');
        }

        if (empty($coursetable)) {
            echo $OUTPUT->notification('External course table not specified.', 'notifyproblem');
        }

        if (empty($cohortcoursetable)) {
            echo $OUTPUT->notification('External cohort enrolment table not specified.', 'notifyproblem');
        }

        if (empty($coursetable) && empty($enroltable)) {
            return;
        }

        $olddebug = $CFG->debug;
        $olddisplay = ini_get('display_errors');
        ini_set('display_errors', '1');
        $CFG->debug = DEBUG_DEVELOPER;
        $olddebugdb = $this->config->debugdb;
        $this->config->debugdb = 1;
        error_reporting($CFG->debug);

        $adodb = $this->db_init();

        if (!$adodb || !$adodb->IsConnected()) {
            $this->config->debugdb = $olddebugdb;
            $CFG->debug = $olddebug;
            ini_set('display_errors', $olddisplay);
            error_reporting($CFG->debug);
            ob_end_flush();

            echo $OUTPUT->notification('Cannot connect the database.', 'notifyproblem');
            return;
        }

        if (!empty($enroltable)) {
            $rs = $adodb->Execute("SELECT *
                                     FROM $enroltable");
            if (!$rs) {
                echo $OUTPUT->notification('Can not read external enrol table.', 'notifyproblem');

            } else if ($rs->EOF) {
                echo $OUTPUT->notification('External enrol table is empty.', 'notifyproblem');
                $rs->Close();

            } else {
                $fieldsobj = $rs->FetchObj();
                $columns = array_keys((array)$fieldsobj);

                echo $OUTPUT->notification('External enrolment table contains following columns:<br />'
                    . implode(', ', $columns), 'notifysuccess');
                $rs->Close();
            }
        }

        if (!empty($coursetable)) {
            $rs = $adodb->Execute("SELECT *
                                     FROM $coursetable");
            if (!$rs) {
                echo $OUTPUT->notification('Can not read external course table.', 'notifyproblem');

            } else if ($rs->EOF) {
                echo $OUTPUT->notification('External course table is empty.', 'notifyproblem');
                $rs->Close();

            } else {
                $fieldsobj = $rs->FetchObj();
                $columns = array_keys((array)$fieldsobj);

                echo $OUTPUT->notification('External course table contains following columns:<br />'
                    . implode(', ', $columns), 'notifysuccess');
                $rs->Close();
            }
        }

        if (!empty($cohortcoursetable)) {
            $rs = $adodb->Execute("SELECT *
                                     FROM $cohortcoursetable");
            if (!$rs) {
                echo $OUTPUT->notification('Can not read external cohort course table.', 'notifyproblem');

            } else if ($rs->EOF) {
                echo $OUTPUT->notification('External cohort course table is empty.', 'notifyproblem');
                $rs->Close();

            } else {
                $fieldsobj = $rs->FetchObj();
                $columns = array_keys((array)$fieldsobj);

                echo $OUTPUT->notification('External cohort course table contains following columns:<br />'
                    . implode(', ', $columns), 'notifysuccess');
                $rs->Close();
            }
        }

        $adodb->Close();

        $this->config->debugdb = $olddebugdb;
        $CFG->debug = $olddebug;
        ini_set('display_errors', $olddisplay);
        error_reporting($CFG->debug);
        ob_end_flush();
    }


    /**
     * Performs a full sync with external database.
     *
     * First it creates new courses if necessary, then
     * enrols and unenrols users.
     *
     * @param progress_trace $trace
     * @return int 0 means success, 1 db connect failure, 4 db read failure
     */
    public function sync_cohorts(progress_trace $trace) {
        global $CFG, $DB;

        // Make sure we sync either enrolments or courses.
        if (!$this->get_config('dbtype') || !$this->get_config('newcohorttable')) {
            $trace->output('Cohorts synchronisation skipped.');
            $trace->finished();
            return 0;
        }

        $trace->output('Starting cohorts synchronisation...');

        // We may need a lot of memory here.
        core_php_time_limit::raise();
        raise_memory_limit(MEMORY_HUGE);

        if (!$extdb = $this->db_init()) {
            $trace->output('Error while communicating with external enrolment database');
            $trace->finished();
            return 1;
        }

        $table = $this->get_config('newcohorttable');
        $cohortname = trim($this->get_config('newcohortname'));
        $cohortdescription = trim($this->get_config('newcohortdescription'));
        $idnumber = trim($this->get_config('newcohortidnumber'));

        // Lowercased versions - necessary because we normalise the resultset with array_change_key_case().
        $cohortnamel = strtolower($cohortname);
        $cohortdescriptionl = strtolower($cohortdescription);
        $idnumberl = strtolower($idnumber);
        $moodle2extnamesmap = array(
            'idnumber' => $idnumberl,
            'name' => $cohortnamel,
            'description' => $cohortdescriptionl
        );

        $updatingcohortsenabled = $this->get_config('enableupdatingcohorts');
        $mappingfield = $this->get_config('updatecohortsmappingfield');  // Idnumber or name.
        $mappingextfieldname = $moodle2extnamesmap[$mappingfield];

        $mappingvalue2extcohort = [];  // accumulate external records

        $createdcount = 0;
        $creationfailedcount = 0;
        $updatedcount = 0;
        $notupdatedcount = 0;
        $invalidcount = 0;

        $extsqlfields = array($cohortname, $cohortdescription, $idnumber);

        $sql = $this->db_get_sql($table, array(), $extsqlfields, true);
        $createcohorts = array();
        require_once("$CFG->dirroot/cohort/lib.php");
        if ($rs = $extdb->Execute($sql)) {
            if (!$rs->EOF) {
                while ($extfields = $rs->FetchRow()) {
                    $extfields = array_change_key_case($extfields, CASE_LOWER);
                    $extfields = $this->db_decode($extfields);
                    if (empty($extfields[$cohortnamel])) {
                        $trace->output('error: invalid external cohort record, cohortname and cohortdescription are mandatory: '
                            . var_export($extfields, true), 1);
                        $invalidcount += 1;
                        continue;
                    }

                    // Save record to accumulator.
                    $mappingvalue2extcohort[$extfields[$mappingextfieldname]] = $extfields;

                    // Check if the cohort is already in Moodle.
                    if ($DB->record_exists('cohort', array($mappingfield => $extfields[$mappingextfieldname]))) {

                        if ($updatingcohortsenabled) {

                            $cohort = $DB->get_record('cohort', array($mappingfield => $extfields[$mappingextfieldname]),
                                'id, name, idnumber, description, contextid');

                            $needupdate = false;
                            foreach (array('name', 'idnumber', 'description') as $fieldname) {
                                $extvalue = $extfields[$moodle2extnamesmap[$fieldname]];
                                if (
                                    // Skip mapping field as key.
                                    $fieldname != $mappingfield
                                    // Value differs.
                                    && $cohort->$fieldname != $extvalue
                                ) {
                                    $cohort->$fieldname = $extvalue;
                                    $needupdate = true;
                                    break;
                                }
                            }

                            if ($needupdate) {
                                cohort_update_cohort($cohort);
                                $trace->output('cohort updated: ' . $extfields[$cohortnamel] . ' [' . $extfields[$idnumberl] . ']', 2);
                                $updatedcount += 1;
                            } else {
                                $notupdatedcount += 1;
                            }
                        }

                        // Already exists, skip.
                        continue;
                    }

                    // // Allow empty idnumber but not duplicates.
                    // if ($idnumber
                    //     && $extfields[$idnumberl] !== ''
                    //     && $extfields[$idnumberl] !== null
                    //     && $DB->record_exists('cohort', array('idnumber' => $extfields[$idnumberl]))) {
                    //     $trace->output('error: duplicate idnumber, can not create cohort: ' . $extfields[$cohortnamel]
                    //         . ' [' . $extfields[$idnumberl] . ']', 1);
                    //     continue;
                    // }
                    $cohort = new stdClass();
                    $cohort->name = $extfields[$cohortnamel];
                    $cohort->description = $extfields[$cohortdescriptionl];
                    $cohort->idnumber = $idnumber ? $extfields[$idnumberl] : '';
                    $createcohorts[] = $cohort;
                }
            }
            $rs->Close();
        } else {
            $extdb->Close();
            $trace->output('Error reading data from the external course table');
            $trace->finished();
            return 4;
        }
        if ($createcohorts) {
            foreach ($createcohorts as $extfields) {
                $newcohort = new stdClass();
                $newcohort->name = $extfields->name;
                $newcohort->description = $extfields->description;
                $newcohort->idnumber = $extfields->idnumber;
                $newcohort->contextid = 1;  // Used to trigger event (?).

                // Don't set the origin of cohort
                // (this prevents the default UI in [Admin > Users > Cohorts] to view and edit cohort members).
                // In this way: `$newcohort->component = 'enrol_poasdatabase';`.

                // Detect duplicate data once again, above we may not find all duplicates
                // in external data using DB collation rules...
                if ($DB->record_exists('cohort', array($mappingfield => $newcohort->$mappingfield))) {
                    $trace->output("can not insert new cohort, duplicate $mappingfield detected: " . $newcohort->$mappingfield, 1);
                    $creationfailedcount += 1;
                    continue;
                }

                $newcohort->id = cohort_add_cohort($newcohort);
                $trace->output("created cohort: $newcohort->id, $newcohort->name, "
                    . "$newcohort->description, $newcohort->idnumber", 1);
                $createdcount += 1;
            }
            unset($createcohorts);
        }

        $trace->output("x Invalid cohorts in ext DB: $invalidcount", 2);
        $trace->output(". Total cohorts not updated: $notupdatedcount", 2);
        $trace->output("* Total cohorts     updated: $updatedcount", 2);
        $trace->output("+ Total cohorts     created: $createdcount", 2);
        $trace->output("x Total cohorts not created: $creationfailedcount", 2);


        $this->remove_cohort_duplicates($trace, $mappingvalue2extcohort);

        // Close db connection.
        $extdb->Close();
        $trace->output('...cohorts synchronisation finished.');
        $trace->finished();

        return 0;
    }

    /** Remove the special case of duplicate cohorts. Say, if
    `updatecohortsmappingfield` option in the plugin's config is set to 'name', then cohorts having the same idnumber but not names are considered duplicates. Ones that not present in external DB should be removed.
     * @param progress_trace $trace
     */
    public function remove_cohort_duplicates(progress_trace $trace, $mappingvalue2extcohort)
    {
        global $CFG, $DB;

        // Check config: if this task enabled.
        $enableremovingduplicatecohorts = $this->get_config('enableremovingduplicatecohorts');

        if (!$enableremovingduplicatecohorts) {
            // Do nothing.
            return;
        }

        require_once("$CFG->dirroot/cohort/lib.php");

        $cohortname = trim($this->get_config('newcohortname'));
        $cohortdescription = trim($this->get_config('newcohortdescription'));
        $idnumber = trim($this->get_config('newcohortidnumber'));

        // Lowercased versions - necessary because we normalise the resultset with array_change_key_case().
        $cohortnamel = strtolower($cohortname);
        $cohortdescriptionl = strtolower($cohortdescription);
        $idnumberl = strtolower($idnumber);
        $moodle2extnamesmap = array(
            'idnumber' => $idnumberl,
            'name' => $cohortnamel,
            'description' => $cohortdescriptionl
        );

        $mappingfield = $this->get_config('updatecohortsmappingfield');  // Idnumber or name (primary field).
        if (!in_array($mappingfield, ['idnumber', 'name'])) {
            $trace->output('Invalid value for  updatecohortsmappingfield setting. Expected `idnumber` or `name` but got: ' . $mappingfield);
            return;
        }
        $mappingextfieldname = $moodle2extnamesmap[$mappingfield];

        // Secondary field: opposite to $mappingfield (primary field).
        $secondaryfield = $mappingfield === 'name' ? 'idnumber' : 'name';
        $secondaryextfieldname = $moodle2extnamesmap[$secondaryfield];

        $deletedcount = 0;

        // Find pairs of cohorts having secondary field the same but primary field different (only `name` & 'idnumber' are considered).
        $sql = "SELECT concat(c1.id, \"-\", c2.id) as _key, c1.id AS id1, c2.id AS id2, c1.idnumber AS idnumber1, c2.idnumber AS idnumber2, c1.name AS name1, c2.name AS name2 FROM  mdl_cohort c1
               JOIN mdl_cohort c2 ON c1.$secondaryfield = c2.$secondaryfield AND c1.$mappingfield < c2.$mappingfield";

        $cohortpairs = $DB->get_records_sql($sql);

        foreach ($cohortpairs as $cohortpair) {
            $secondaryvalue = $cohortpair->{$secondaryfield . '1'};

            // remove cohorts: inappropriate duplicates.
            $cohortids = [];
            $cohortidstokeep = [];

            foreach (['1', '2'] as $suffix) {

                $cohortids [] = $cohortpair->{'id' . $suffix};
                $mappingvalue = $cohortpair->{$mappingfield . $suffix};

                if (array_key_exists($mappingvalue, $mappingvalue2extcohort) && $mappingvalue2extcohort[$mappingvalue][$secondaryextfieldname] == $secondaryvalue) {
                    // Remember that this cohort should remain as it's still present in ext source.
                    $cohortidstokeep [] = $cohortpair->{'id' . $suffix};
                }
            }

            // Ensure that at least one of cohorts in the group remains.
            if (!empty($cohortidstokeep)) {
                // Using MINUS operation: omit cohort that present in ext DB from the following removal.
                foreach (array_diff($cohortids, $cohortidstokeep) as $cohortid) {

                    $cohort = $DB->get_record('cohort', ['id' => $cohortid]);

                    $trace->output("Removing duplicate cohort: (id=$cohort->id) $cohort->name [$cohort->idnumber]", 2);

                    // Call API to delete cohort.
                    cohort_delete_cohort($cohort);

                    $deletedcount += 1;
                }
            }
        }
        $trace->output('... Deleted cohort duplicates: ' . $deletedcount, 1);
    }

    /** Find cohort in DB by its idnumber and return its id.
     * @param $cohortidnumber
     * @return int cohort id
     * @throws dml_exception
     */
    public function get_cohort_id_by_idnumber($cohortidnumber) {
        global $DB;
        $cohort = $DB->get_record('cohort', array('idnumber' => $cohortidnumber));
        return $cohort->id;
    }

    /** Find cohort by its idnumber and return the DB record.
     * @param $cohortidnumber
     * @return false|mixed|stdClass DB record - cohort
     * @throws dml_exception
     */
    public function get_cohort_by_idnumber($cohortidnumber) {
        global $DB;
        $cohort = $DB->get_record('cohort', array('idnumber' => $cohortidnumber));
        return $cohort;
    }

    /** Second entry of sync_cohorts sсheduled task.
     * Calculate diff of existing cohort members and requested members (from ext db) and perform the sync.
     * @param progress_trace $trace
     * @return int
     * @throws dml_exception
     */
    public function sync_user_cohorts(progress_trace $trace) {
        global $CFG, $DB;

        // Make sure we sync either enrolments or courses.
        if (!$this->get_config('dbtype') || !$this->get_config('usercohorttable')) {
            $trace->output('User-cohorts synchronisation skipped.');
            $trace->finished();
            return 0;
        }

        $trace->output('Starting user-cohort synchronisation...');

        // We may need a lot of memory here.
        core_php_time_limit::raise();
        raise_memory_limit(MEMORY_HUGE);

        if (!$extdb = $this->db_init()) {
            $trace->output('Error while communicating with external enrolment database');
            $trace->finished();
            return 1;
        }

        $unenrolusercohortaction = $this->get_config('unenrolusercohortaction');

        $table = $this->get_config('usercohorttable');
        $cohortidnumber = trim($this->get_config('cohortidnumber'));
        $userlogin = trim($this->get_config('userlogin'));

        // Lowercased versions - necessary because we normalise the resultset with array_change_key_case().
        $cohortidnumberl = strtolower($cohortidnumber);
        $userloginl = strtolower($userlogin);

        $cohortidnumber2usernames = [];  // Mapping: cohortidnumber => [user login].

        $sqlfields = array($cohortidnumber, $userlogin);
        $counter = 0;
        $sql = $this->db_get_sql($table, array(), $sqlfields, true);
        require_once("$CFG->dirroot/cohort/lib.php");
        if ($rs = $extdb->Execute($sql)) {
            if (!$rs->EOF) {
                while ($fields = $rs->FetchRow()) {
                    $counter++;
                    $fields = array_change_key_case($fields, CASE_LOWER);
                    $fields = $this->db_decode($fields);
                    if (empty($fields[$cohortidnumberl]) || empty($fields[$userloginl])) {
                        $trace->output('error: invalid external record: empty field in ' . var_export($fields, true), 1);
                        continue;
                    }

                    $cohortidnumber = $fields[$cohortidnumberl];
                    $useridlogin = $fields[$userlogin];
                    $cohortidnumber2usernames[$cohortidnumber][$useridlogin] = $useridlogin;  // Set keys same as values.
                }
            }
            $rs->Close();
        } else {
            $extdb->Close();
            $trace->output('Error reading data from the external course table');
            $trace->finished();
            return 4;
        }

        $trace->output("External db '$table' has rows: $counter", 1);

        // Use efficient SELECTs ...

        // Find all users in all cohorts (for empty cohorts, user's columns will be null !).
        $sql = 'SELECT concat(IFNULL(u.id, 0), "@", c.id) as _key, c.id, c.idnumber, u.id as userid, u.username
               FROM {cohort} c
               LEFT JOIN {cohort_members} cm ON c.id = cm.cohortid
               LEFT JOIN {user} u ON u.id = cm.userid
              WHERE c.visible = 1 AND (u.deleted is null or u.deleted = 0)';

        $cohortsusers = $DB->get_records_sql($sql);

        $existingcohortmembers = [];
        $existingusercohorts = [];
        $username2id = [];
        $cohortidnumber2id = [];
        $userid2cohortsremovefrom = [];  // Mapping: userid => [cohortid].

        // Write to dict-like arrays indexed by ext db values.
        foreach ($cohortsusers as $cu) {
            $cohortidnumber2id[$cu->idnumber] = $cu->id;
            if ($cu->username) {
                $existingcohortmembers[$cu->idnumber][$cu->username] = $cu;
                $existingusercohorts[$cu->username][$cu->idnumber] = $cu;
                $username2id[$cu->username] = $cu->userid;
            } else {
                // Empty cohort.
                // User does not exist, but null keys are not allowed in php (int and string only).
                $existingcohortmembers[$cu->idnumber][''] = $cu;
            }
        }
        unset($cohortsusers);

        $ncohorts = count($existingcohortmembers);
        $nallcohorts = count($cohortidnumber2id);
        $nusers = count($existingusercohorts);
        $trace->output("Currently, $nusers users are in $ncohorts cohorts (total cohorts: $nallcohorts).", 3);

        $badusernames = [];

        // Iterate over known cohorts, remember deleted members and add new members ...

        foreach ($existingcohortmembers as $cohortidnumber => $usernames) {

            $extusernames = $cohortidnumber2usernames[$cohortidnumber];

            $cohortid = $cohortidnumber2id[$cohortidnumber];

            $existingmembers = $existingcohortmembers[$cohortidnumber];
            $newusernames = array_diff_key($extusernames, $existingmembers);  // MINUS operation on sets of keys.

            $deletedusernames = array_diff_key($existingmembers, $extusernames);

            foreach ($deletedusernames as $username => $cu) {
                if (!$username) {
                    continue;  // Skip '' entries.
                }
                $userid2cohortsremovefrom[$cu->userid][$cohortid] = $cohortid;

                $trace->output("- Detected member removed from cohort: "
                    . "$cu->userid ($username) --from-- $cohortid ($cohortidnumber).", 1);
            }

            // Handle $new_usernames.
            foreach ($newusernames as $username) {
                if (!$username) {
                    continue;  // Skip '' entries.
                }
                // Retrieve userid.
                if (empty($userid = $username2id[$username])) {
                    if ($badusernames[$username]) {
                        continue;
                    }
                    $user = $DB->get_record('user', ['username' => $username]);
                    if ($user) {
                        $userid = $username2id[$username] = $user->id;
                    } else {
                        $trace->output("Cannot find user with username: $username", 2);
                        $badusernames[$username] = $username;
                        continue;
                    }
                }

                cohort_add_member($cohortid, $userid);
                $trace->output("+ added user to cohort: $userid --> $cohortid ($username --> $cohortidnumber)", 1);
            }
        }

        $deletedcohortidnumbers = array_diff_key($existingcohortmembers, $cohortidnumber2usernames);
        $deletedcohortids = array_map(function ($idnumber) use ($cohortidnumber2id) {
            return $cohortidnumber2id[$idnumber];
        }, $deletedcohortidnumbers);

        $this->clear_user_cohorts($trace, $userid2cohortsremovefrom, $deletedcohortids);

        // Close db connection.
        $extdb->Close();
        $trace->output('...cohorts synchronisation finished.');
        $trace->finished();

        return 0;
    }

    /** Helper to retrieve tags for given idnumber from external DB.
     * @param string $idnumber target idnumber
     * @param object $extdb
     * @param string $table tags table
     * @param string $sqlidnumberfield field name in ext db table: idnumber
     * @param string $sqltagnamefield field name in ext db table: tag
     * @return array
     */
    public function get_tag_by_idnumber($idnumber, $extdb, $table, $sqlidnumberfield, $sqltagnamefield) {
        $sqlfields = array($sqlidnumberfield, $sqltagnamefield);
        $sql = $this->db_get_sql($table, array($sqlidnumberfield => $idnumber), $sqlfields, true);
        $restags = array();
        if ($rs = $extdb->Execute($sql)) {
            if (!$rs->EOF) {
                while ($fields = $rs->FetchRow()) {
                    $fields = array_change_key_case($fields, CASE_LOWER);
                    $fields = $this->db_decode($fields);

                    $restags[] = $fields[$sqltagnamefield];
                }
            }
        }

        return $restags;
    }

    /** Remove users from cohorts and whole cohorts as necessary.
     * @param progress_trace $trace
     * @param array $userid2cohortsremovefrom mapping of user id to cohorts ids to remove the user from
     * @param array $deletedcohortids ids of cohorts to remove completely
     * @return int
     * @throws dml_exception
     */
    public function clear_user_cohorts(progress_trace $trace, $userid2cohortsremovefrom, $deletedcohortids): int {
        global $CFG, $DB;

        // Make sure we sync either enrolments or courses.
        if (!$this->get_config('dbtype') || !$this->get_config('usercohorttable')) {
            $trace->output('User-cohorts cleaning skipped (no source table specified in config).');
            $trace->finished();
            return 0;
        }

        // Follow config.
        $unenrolcohortaction = $this->get_config('unenrolcohortaction');
        $keepenrolledcohorts = $this->get_config('keepenrolledcohorts');
        $unenrolusercohortaction = $this->get_config('unenrolusercohortaction');
        $allowremovecohort = $unenrolcohortaction == ENROL_EXT_REMOVED_UNENROL;
        $allowremoveuserfromcohort = $unenrolusercohortaction == ENROL_EXT_REMOVED_UNENROL;

        if (!$allowremoveuserfromcohort && !$allowremovecohort) {
            $trace->output('Cleaning [unenrolled] cohorts disabled, skip it.');
            return 0;
        }

        if ((!$allowremoveuserfromcohort
                || empty($userid2cohortsremovefrom))
            && (!$allowremovecohort
                || empty($deletedcohortids))) {
            $trace->output('Nothing to remove [from] cohort(s).', 1);
            return 0;
        }

        $trace->output('Starting user-cohorts cleaning...');

        require_once("$CFG->dirroot/cohort/lib.php");

        if ($allowremovecohort) {
            foreach ($deletedcohortids as $deletedcohortid) {

                if ($keepenrolledcohorts) {
                    // Check for any enrolments the cohort has ...
                    $cohortenrolled = $DB->record_exists('enrol', array(
                        'enrol' => "cohort",
                        // Any 'status'.
                        // Any 'courseid'.
                        // Any 'roleid'.
                        'customint1' => $deletedcohortid,
                        // Any groupid ('customint2' => ...).
                    ));

                    if ($cohortenrolled) {
                        // Don't change flags: do nothing.
                        // Refused to remove cohort as it has enrolments.
                        continue;
                    }
                }

                $cohort = $DB->get_record('cohort', ['id' => $deletedcohortid]);

                // Perform removal.
                cohort_delete_cohort($cohort);
                $trace->output('removed cohort ' . $cohort->idnumber . ' since it has no enrolments.');
            }
        }

        if ($allowremoveuserfromcohort) {
            foreach ($userid2cohortsremovefrom as $userid => $cohortids) {
                foreach ($cohortids as $cohortid) {
                    // What if user is not in remote DB ??

                    {
                        // Perform removal.
                        $trace->output("removing user $userid from cohort $cohortid", 3);
                        cohort_remove_member($cohortid, $userid);
                    }
                }

                $ncohorts = count($cohortids);
                $trace->output("removed user with id: $userid from $ncohorts cohort(s).", 2);
            }
        }

        $trace->output('...cohorts cleaning finished.');
        $trace->finished();

        return 0;
    }

    /** Entry function of scheduled_task `sync_courses_cohorts`.
     * @param progress_trace $trace
     * @return int|void
     * @throws coding_exception
     * @throws dml_exception
     * @throws moodle_exception
     * @throws required_capability_exception
     */
    public function sync_groups_courses(progress_trace $trace) {
        global $CFG, $DB;

        // Make sure we sync either enrolments or courses.
        if (!$this->get_config('dbtype') || !$this->get_config('groupcohortcoursestable')) {
            $trace->output('Courses-cohorts synchronisation skipped.');
            $trace->finished();
            return 0;
        }

        $trace->output('Starting courses-cohorts synchronisation...');

        // We may need a lot of memory here.
        core_php_time_limit::raise();
        raise_memory_limit(MEMORY_HUGE);

        if (!$extdb = $this->db_init()) {
            $trace->output('Error while communicating with external enrolment database');
            $trace->finished();
            return 1;
        }

        $table = $this->get_config('groupcohortcoursestable');
        $cohortidnum = trim($this->get_config('groupcohortidnumber'));
        $courseidnumber = trim($this->get_config('courseidnumber'));
        $groupname = trim($this->get_config('groupname'));
        $cohortmemberrole = trim($this->get_config('remotecohortmemberrole'));

        $localrolefield = $this->get_config('localrolefield');
        $defaultrole = $this->get_config('defaultrole');

        // Lowercased versions - necessary because we normalise the resultset with array_change_key_case().
        $cohortidnuml = strtolower($cohortidnum);
        $courseidnumberl = strtolower($courseidnumber);
        $groupnamel = strtolower($groupname);
        $cohortmemberrolel = strtolower($cohortmemberrole);

        // Create roles mapping.
        $allroles = get_all_roles();
        if (!isset($allroles[$defaultrole])) {
            $defaultrole = 0;
        }
        $roles = array();
        foreach ($allroles as $role) {
            $roles[$role->$localrolefield] = $role->id;
        }

        // Note: cohortmemberrole column may not present in remote table (until full migration of production DB).
        $sqlfields = array($cohortidnum, $courseidnumber, $groupname /*,$cohortmemberrole*/);


        $unenrolcohortaction = $this->get_config('unenrolcohortaction');

        if ($unenrolcohortaction == ENROL_EXT_REMOVED_KEEP) {

            $sql = $this->db_get_sql($table, array(), null /*$sqlfields*/, true);

            if ($rs = $extdb->Execute($sql)) {
                if (!$rs->EOF) {
                    while ($fields = $rs->FetchRow()) {
                        $fields = array_change_key_case($fields, CASE_LOWER);
                        $fields = $this->db_decode($fields);

                        if (empty($fields[$cohortidnuml])) {
                            $trace->output('error: invalid external cohort record, cohort idnumber, name and cohortdescription '
                                . 'are mandatory:' . var_export($fields, true), 1);
                            continue;
                        }

                        $course = $this->get_course_by_idnumber($fields[$courseidnumberl]);
                        $groupname = trim($fields[$groupnamel]);

                        $cohortid = $this->get_cohort_id_by_idnumber($fields[$cohortidnuml]);
                        if ($DB->record_exists('cohort', array('id' => $cohortid))) {
                            ; // Ok.
                        } else {
                            $trace->output('Cohort not found! (1) idnumber: ' . ($fields[$cohortidnuml]), 1);
                            continue;
                        }

                        $field = array(
                            'customint2' => COHORT_CREATE_GROUP,
                            'customint1' => $cohortid,
                            'roleid' => $roles[$fields[$cohortmemberrolel]] ?? $defaultrole
                        );

                        if (!$course) {
                            $trace->output('course not found ' . $fields[$courseidnumberl], 1);
                            continue;
                        }

                        if ($this->add_course_cohort($course, $field, $groupname) != null) {
                            $trace->output('    added to course : ' . $fields[$cohortidnuml]
                                . "  (" . $groupname . ") -> " . $course->fullname . ' (as '.$field['roleid'].')', 1);
                        }
                    }
                }
                $rs->Close();
            } else {
                $extdb->Close();
                $trace->output('Error reading data from the external course-cohort table');
                $trace->finished();
                return 4;
            }
        } else {  // When ($unenrolcohortaction != ENROL_EXT_REMOVED_KEEP).

            // Mappings: [course][cohort] => entry.
            $existing = array(); // Course-cohort enrolments currently in Moodle.
            $external = array(); // Course-cohort enrolments in external database.

            $existingcount = 0;
            $existingactivecount = 0;
            $externalcount = 0;

            // First find all existing course-cohort enrol instances.
            $sql = "SELECT c.id AS courseid, c.visible, c.idnumber, c.shortname,
                e.id AS enrolid, e.status AS enrol_status, e.customint1 AS cohortid,
                e.customint2 AS groupid, h.idnumber as cohort_idnumber
                      FROM {course} c
                      INNER JOIN {enrol} e ON (e.courseid = c.id AND e.enrol = 'cohort')
                      INNER JOIN {cohort} h ON (e.customint1 = h.id)";
            $rs = $DB->get_recordset_sql($sql); // Watch out for idnumber duplicates.

            foreach ($rs as $course) {
                if (empty($course->idnumber) || empty($course->enrolid)) {
                    continue;
                }
                // Check for `enrol` duplicates.
                if (isset($existing[$course->idnumber][$course->cohort_idnumber])) {
                    $trace->output("Found duplicate enrol instance for course-cohort: "
                        . "'$course->idnumber' - '$course->cohort_idnumber'.", 2);

                    $todeleteenrolid = $course->enrolid;
                    $prevcourse = $existing[$course->idnumber][$course->cohort_idnumber];
                    if ($prevcourse->groupid == 0) {
                        // Previously recorded enrol has no group.
                        $todeleteenrolid = $prevcourse->enrolid;
                        // Replace current the slot with current record.
                        $existing[$course->idnumber][$course->cohort_idnumber] = $course;
                    }

                    // Search for 'enrol' record (to delete).
                    $enrolinstance = $DB->get_record('enrol', array(
                        'id' => $todeleteenrolid
                    ));

                    // Delete enrolment.
                    if ($enrolinstance != null) {
                        $plugin = enrol_get_plugin('cohort');
                        $plugin->delete_instance($enrolinstance);
                        $trace->output("cohort enrolment duplicate was removed: "
                            . "$course->cohort_idnumber ==> $course->shortname .", 3);
                    } else {
                        $trace->output("not found cohort enrolment to delete ...", 3);
                    }

                    continue;  // Do not process the removed entry.
                }

                if (!isset($existing[$course->idnumber])) {
                    $existing[$course->idnumber] = array();
                }
                $existing[$course->idnumber][$course->cohort_idnumber] = $course;

                // Counting for report.
                $existingcount += 1;
                if ($course->enrol_status == ENROL_INSTANCE_ENABLED) {
                    $existingactivecount += 1;
                }
            }
            $rs->close();

            $trace->output("Found $existingcount existing course-cohort enrol instances,"
                . " including $existingactivecount active.", 1);

            // Second, find all remote course-cohort entries.
            $sql = $this->db_get_sql($table, array(), null /*$sqlfields*/, true);

            if ($rs = $extdb->Execute($sql)) {
                if (!$rs->EOF) {
                    while ($fields = $rs->FetchRow()) {
                        $fields = array_change_key_case($fields, CASE_LOWER);
                        $fields = $this->db_decode($fields);

                        if (empty($fields[$cohortidnuml]) || empty($fields[$courseidnumberl])) {
                            $trace->output('error: invalid external course-cohort record,'
                                . ' cohort idnumber and course idnumber are mandatory: ' . var_export($fields, true), 1);
                            continue;
                        }

                        $entry = new stdClass();
                        $entry->idnumber = $fields[$courseidnumberl];
                        $entry->cohort_idnumber = $fields[$cohortidnuml];
                        $entry->groupname = trim($fields[$groupnamel]);
                        $entry->cohortmemberrole = trim($fields[$cohortmemberrolel]);

                        if (!isset($external[$entry->idnumber])) {
                            $external[$entry->idnumber] = array();
                        }

                        $external[$entry->idnumber][$entry->cohort_idnumber] = $entry;
                        $externalcount += 1;
                    }
                }
                $rs->Close();
            } else {
                $extdb->Close();
                $trace->output('Error reading data from the external course-cohort table');
                $trace->finished();
                return 4;
            }

            $trace->output("Found $externalcount external course-cohort enrol requests.", 1);

            // Third, find out the differences between existing and external course-cohort enrolments.
            $extremoved = array(); // Course-cohort enrolments removed from external database.

            $regexpattern = trim($this->get_config('course_idnumber_regex_capturing_teacher'));

            foreach ($existing as $courseidnumber => $existingbycohort) {

                if ($regexpattern && 1 != preg_match($regexpattern, $courseidnumber)) {
                    continue;  // Skip the course as it is not related to our plugin.
                }

                if (empty($external[$courseidnumber])) {
                    // Cohorts for this course were removed completely.
                    $trace->output("cohorts for this course were removed completely - course idnumber: $courseidnumber", 3);

                    $extremoved = array_merge($extremoved, array_values($existingbycohort));

                    continue;
                }

                // Find removed among existing entries.
                foreach ($existingbycohort as $cohortidnumber => $existingentry) {
                    if (empty($external[$courseidnumber][$cohortidnumber])) {
                        // This cohort was removed.
                        $trace->output("this cohort was removed - "
                            . "cohort idnumber: $cohortidnumber, course idnumber: $courseidnumber", 3);

                        $extremoved[] = $existingentry;
                    }
                }
            }

            $trace->output('Detected ' . count($extremoved)
                . " existing course-cohort enrol instances that were removed from external database.", 1);

            // Forth, apply newly added external enrolments.
            foreach ($external as $courseidnumber => $externalbycohort) {

                // Create/refresh all external entries for this course.
                foreach ($externalbycohort as $cohortidnumber => $externalentry) {

                    // Skip existing instances that are already active.
                    if (isset($existing[$courseidnumber]) && isset($existing[$courseidnumber][$cohortidnumber])) {

                        $entry = $existing[$courseidnumber][$cohortidnumber];
                        if ($entry->enrol_status == ENROL_INSTANCE_ENABLED) {
                            continue;
                        }
                        unset($entry);
                    }

                    // Create or activate course-cohort enrolment.
                    $course = $this->get_course_by_idnumber($externalentry->idnumber);
                    $groupname = $externalentry->groupname;

                    $cohortid = $this->get_cohort_id_by_idnumber($externalentry->cohort_idnumber);
                    if ($DB->record_exists('cohort', array('id' => $cohortid))) {
                        ; // Nothing to do.
                    } else {
                        $trace->output("Cohort not found! (2) idnumber:  $externalentry->cohort_idnumber", 1);
                        continue;
                    }

                    $rolename = property_exists($externalentry, 'cohortmemberrole') ? $externalentry->cohortmemberrole : '';

                    $field = array(
                        'customint1' => $cohortid,
                        'customint2' => COHORT_CREATE_GROUP,
                        'roleid' => $roles[$rolename] ?? $defaultrole
                    );

                    if ($course == false) {
                        $trace->output('course not found: idnumber: ' . $externalentry->idnumber, 1);
                        continue;
                    }

                    if ($this->add_course_cohort($course, $field, $groupname) != null) {
                        $trace->output("added to course : $externalentry->cohort_idnumber  ($groupname) -> $course->fullname" . ' (as '.$field['roleid'].')', 3);
                    }  // Else - report an error?
                }
            }

            // Fifth, deal with externally removed course-cohort enrolments.
            $plugin = enrol_get_plugin('cohort');

            foreach ($extremoved as $removedentry) {

                // Search for 'enrol' record.
                $enrolinstance = $DB->get_record('enrol', array(
                    'id' => $removedentry->enrolid
                ));

                if ($unenrolcohortaction == ENROL_EXT_REMOVED_SUSPEND) {

                    if ($enrolinstance->status != ENROL_INSTANCE_DISABLED) {
                        // Deactivate enrolment.
                        $plugin->update_status($enrolinstance, ENROL_INSTANCE_DISABLED);
                        $trace->output("cohort enrolment suspended: $removedentry->cohort_idnumber ==> $removedentry->shortname", 1);
                    }
                } else if ($unenrolcohortaction == ENROL_EXT_REMOVED_UNENROL) {

                    $plugin->delete_instance($enrolinstance);
                    $trace->output("cohort was fully unenrolled: $removedentry->cohort_idnumber ==> $removedentry->shortname", 1);
                } else {

                    $trace->output("Unknown unenrolcohortaction: $unenrolcohortaction ...", 1);
                }
            }
        }
    }

    /** Find course by its idnumber
     * @param string $courseidnumber
     * @return false|mixed|stdClass course record from DB
     * @throws dml_exception
     */
    public function get_course_by_idnumber($courseidnumber) {
        global $DB;
        return $DB->get_record('course', array('idnumber' => $courseidnumber));
    }

    /**
     * Create a new group with the cohorts name.
     *
     * @param int $courseid
     * @param int $cohortid
     * @param string $groupnameinput
     * @return int $groupid Group ID for this cohort.
     * @throws dml_exception
     * @throws moodle_exception
     */
    public function enrol_cohort_get_or_create_group($courseid, $cohortid, $groupnameinput) {
        global $DB, $CFG;

        require_once($CFG->dirroot . '/group/lib.php');

        $groupname = $groupnameinput;

        $groupid = 0;
        // Check to see if the cohort group name already exists. Add an incremented number if it does.
        if ($DB->record_exists('groups', array('name' => $groupname, 'courseid' => $courseid))) {
            $groupid = groups_get_group_by_name($courseid, $groupname);
            if ($groupid === false) {
                ; // Wierd~... Must always present.
            } else {
                print("    A group was reused: $groupname, id: $groupid;");
            }
        }
        if (!$groupid) {
            // Create a new group for the cohort.
            $groupdata = new stdClass();
            $groupdata->courseid = $courseid;
            $groupdata->name = $groupname;
            $groupid = groups_create_group($groupdata);

            print("    A group was created: $groupname, id: $groupid; ");
        }
        return $groupid;
    }

    /**
     * Create or find existing instance of "cohort" enrol plugin.
     * (see /enrol/cohort/lib.php)
     * Existing instance should match in following fields:
     * - enrol  (= "cohort")
     * - status (can be ENROL_INSTANCE_ENABLED or ENROL_INSTANCE_DISABLED)
     * - courseid
     * - roleid
     * - customint1  (cohort id)
     * - customint2  (group id)
     * @param object $course
     * @param array|null $fields instance fields:
     *              $array(
     *                  'customint1' => cohortid,
     *                  'customint2' => groupid or COHORT_CREATE_GROUP
     *              );
     * @param string $inputname - name of group created
     * @param int $defaultroleid - role for enrolment if not set in $fields. By default: 5 - student role in Moodle
     * @return int id of existing or new instance, null if one can not be created
     * @throws coding_exception
     * @throws dml_exception
     * @throws moodle_exception
     * @throws required_capability_exception
     */
    public function add_course_cohort($course, array $fields, string $inputname, int $defaultroleid = 5): ?int
    {
        global $DB, $CFG;

        // Find a group in the course by group name, to reuse if it exists.
        if (!empty($fields['customint2']) && $fields['customint2'] == COHORT_CREATE_GROUP) {
            // Create a new group for the cohort if requested.
            $context = context_course::instance($course->id);

            require_capability('moodle/course:managegroups', $context);
            $groupid = $this->enrol_cohort_get_or_create_group(
                $course->id,
                /*cohortid=*/ $fields['customint1'],
                /*groupname=*/ $inputname);

            $fields['customint2'] = $groupid;
        }

        $plugin = enrol_get_plugin('cohort');
        if (!$fields['roleid'])
            $fields['roleid'] = $defaultroleid;  // Set default role.

        if ($fields['customint2'] != null) {

            // Search 'enrol' record for this course & group.
            $cohortinstance = $DB->get_record('enrol', array(
                'enrol' => "cohort",
                // Don't restrict: `'status'=>ENROL_INSTANCE_ENABLED,`.
                'courseid' => $course->id,
                'roleid' => $fields['roleid'],
                'customint1' => $fields['customint1'],
                'customint2' => $fields['customint2'],
            ));

            if ($cohortinstance) {
                // The instance was reused.

                if ($cohortinstance->status != ENROL_INSTANCE_ENABLED) {
                    // Activate enrolment.
                    $plugin->update_status($cohortinstance, ENROL_INSTANCE_ENABLED);
                    print("  cohort enrolment activated: $cohortinstance->cohortname ==> $course->shortname -");
                }
            } else {
                // An instance created.
                $cohortinstance = $plugin->add_instance($course, $fields);
            }
            return $cohortinstance;
        }

        return null;
    }
}



/*
Useful for viewing/modifing status in user_enrolments table
Current user enrolment status numbers:

0 = ENROL_USER_ACTIVE: approved and active enrolments
1 = ENROL_USER_SUSPENDED: pending enrolment applications
2 = ENROL_APPLY_USER_WAIT: enrolment applications on waiting list

*/


/* definitions from lib/enrollib.php :

* When user disappears from external source, the enrolment is completely removed
define('ENROL_EXT_REMOVED_UNENROL', 0);

* When user disappears from external source, the enrolment is kept as is - one way sync
define('ENROL_EXT_REMOVED_KEEP', 1);

*
 * When user disappears from external source, user enrolment is suspended, roles are kept as is.
 * In some cases user needs a role with some capability to be visible in UI - suc has in gradebook,
 * assignments, etc.

define('ENROL_EXT_REMOVED_SUSPEND', 2);

*
 * When user disappears from external source, the enrolment is suspended and roles assigned
 * by enrol instance are removed. Please note that user may "disappear" from gradebook and other areas.
 *
define('ENROL_EXT_REMOVED_SUSPENDNOROLES', 3);

*/
