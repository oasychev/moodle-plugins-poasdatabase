<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Database enrolment plugin settings and presets.
 *
 * @package    enrol_poasdatabase
 * @copyright  2023 Oleg Sychev
 * @copyright  based on work by 2010 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    // General settings.
    $settings->add(new admin_setting_heading('enrol_poasdatabase_settings', '',
        get_string('pluginname_desc', 'enrol_poasdatabase')));

    $settings->add(new admin_setting_heading('enrol_poasdatabase_exdbheader',
        get_string('settingsheaderdb', 'enrol_poasdatabase'), ''));

    $options = array('', "access", "ado_access", "ado", "ado_mssql",
        "borland_ibase", "csv", "db2", "fbsql", "firebird", "ibase", "informix72", "informix",
        "mssql", "mssql_n", "mssqlnative", "mysql", "mysqli", "mysqlt",
        "oci805", "oci8", "oci8po", "odbc", "odbc_mssql", "odbc_oracle", "oracle",
        "pdo", "postgres64", "postgres7", "postgres", "proxy", "sqlanywhere", "sybase", "vfp");
    $options = array_combine($options, $options);
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/dbtype',
        get_string('dbtype', 'enrol_poasdatabase'),
        get_string('dbtype_desc', 'enrol_poasdatabase'), '', $options));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbhost', get_string('dbhost', 'enrol_poasdatabase'),
        get_string('dbhost_desc', 'enrol_poasdatabase'), 'localhost'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbuser', get_string('dbuser', 'enrol_poasdatabase'), '', ''));

    $settings->add(new admin_setting_configpasswordunmask('enrol_poasdatabase/dbpass',
        get_string('dbpass', 'enrol_poasdatabase'), '', ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbname', get_string('dbname', 'enrol_poasdatabase'),
        get_string('dbname_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbencoding',
        get_string('dbencoding', 'enrol_poasdatabase'), '', 'utf-8'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbsetupsql',
        get_string('dbsetupsql', 'enrol_poasdatabase'),
        get_string('dbsetupsql_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/dbsybasequoting',
        get_string('dbsybasequoting', 'enrol_poasdatabase'),
        get_string('dbsybasequoting_desc', 'enrol_poasdatabase'), 0));

    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/debugdb',
        get_string('debugdb', 'enrol_poasdatabase'),
        get_string('debugdb_desc', 'enrol_poasdatabase'), 0));



    $settings->add(new admin_setting_heading('enrol_poasdatabase_localheader',
        get_string('settingsheaderlocal', 'enrol_poasdatabase'), ''));

    $options = array('id' => 'id', 'idnumber' => 'idnumber', 'shortname' => 'shortname');
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/localcoursefield',
        get_string('localcoursefield', 'enrol_poasdatabase'), '', 'idnumber', $options));

    $options = array('id' => 'id', 'idnumber' => 'idnumber', 'email' => 'email', 'username' => 'username');
    // Only local users if username selected, no mnet users!
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/localuserfield',
        get_string('localuserfield', 'enrol_poasdatabase'), '', 'idnumber', $options));

    $options = array('id' => 'id', 'shortname' => 'shortname');
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/localrolefield',
        get_string('localrolefield', 'enrol_poasdatabase'), '', 'shortname', $options));

    $options = array('id' => 'id', 'idnumber' => 'idnumber');
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/localcategoryfield',
        get_string('localcategoryfield', 'enrol_poasdatabase'), '', 'id', $options));


    $settings->add(new admin_setting_heading('enrol_poasdatabase_remoteheader',
        get_string('settingsheaderremote', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remoteenroltable',
        get_string('remoteenroltable', 'enrol_poasdatabase'),
        get_string('remoteenroltable_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remotecoursefield',
        get_string('remotecoursefield', 'enrol_poasdatabase'),
        get_string('remotecoursefield_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remoteuserfield',
        get_string('remoteuserfield', 'enrol_poasdatabase'),
        get_string('remoteuserfield_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remoterolefield',
        get_string('remoterolefield', 'enrol_poasdatabase'),
        get_string('remoterolefield_desc', 'enrol_poasdatabase'), ''));

    $otheruserfieldlabel = get_string('remoteotheruserfield', 'enrol_poasdatabase');
    $otheruserfielddesc  = get_string('remoteotheruserfield_desc', 'enrol_poasdatabase');
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remoteotheruserfield',
        $otheruserfieldlabel, $otheruserfielddesc, ''));

    if (!during_initial_install()) {
        $options = get_default_enrol_roles(context_system::instance());
        $student = get_archetype_roles('student');
        $student = reset($student);
        $settings->add(new admin_setting_configselect('enrol_poasdatabase/defaultrole',
            get_string('defaultrole', 'enrol_poasdatabase'),
            get_string('defaultrole_desc', 'enrol_poasdatabase'), $student->id, $options));
    }

    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/ignorehiddencourses',
        get_string('ignorehiddencourses', 'enrol_poasdatabase'),
        get_string('ignorehiddencourses_desc', 'enrol_poasdatabase'), 0));

    $options = array(ENROL_EXT_REMOVED_UNENROL        => get_string('extremovedunenrol', 'enrol'),  // Unenrol user from course.
                     ENROL_EXT_REMOVED_KEEP           => get_string('extremovedkeep', 'enrol'),     // Keep user enrolled.
                     ENROL_EXT_REMOVED_SUSPEND        => get_string('extremovedsuspend', 'enrol'),  // Disable course enrolment.
                     ENROL_EXT_REMOVED_SUSPENDNOROLES => get_string('extremovedsuspendnoroles', 'enrol'));  // Disable
                            // course enrolment and remove roles.

    // External unenrol action | Select action to perform when user enrolment disappears from external enrolment source.
    // Please note that some user data and settings
    // are purged from course during course unenrolment.
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/unenrolaction',
        get_string('extremovedaction', 'enrol'),
        get_string('extremovedaction_help', 'enrol'), ENROL_EXT_REMOVED_SUSPEND, $options));

    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/allow_reenabling_teachers',
        get_string('allow_reenabling_teachers', 'enrol_poasdatabase'),
        get_string('allow_reenabling_teachers_desc', 'enrol_poasdatabase'), 1));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/course_idnumber_regex_capturing_teacher',
        get_string('course_idnumber_regex_capturing_teacher', 'enrol_poasdatabase'),
        get_string('course_idnumber_regex_capturing_teacher_desc', 'enrol_poasdatabase'), ''));
        // Regex pattern example : '/\A\d+\+\d+(?:\.\d+){2,}-\d{8,}\*\d+\/(\d{8,})\s[ОЗ]{2,4}_[НС]{2,}\Z/' .


    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/allow_unenrol_2nd_teachers',
        get_string('allow_unenrol_2nd_teachers', 'enrol_poasdatabase'),
        get_string('allow_unenrol_2nd_teachers_desc', 'enrol_poasdatabase'), 0));



    $settings->add(new admin_setting_heading('enrol_poasdatabase_newcategories',
        get_string('settingsheadernewcategories', 'enrol_poasdatabase'), ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remotecategoriestable',
        get_string('remotecategoriestable', 'enrol_poasdatabase'),
        get_string('remotecategoriestable_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcategoryname',
        get_string('remotecategorynamefield', 'enrol_poasdatabase'), '', 'categoryname'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcategoryidnumber',
        get_string('remotecategoryidnumberfield', 'enrol_poasdatabase'), '', 'idnumber'));




    $settings->add(new admin_setting_heading('enrol_poasdatabase_newcohortsheader',
        get_string('settingsheadernewcohorts', 'enrol_poasdatabase'), ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcohorttable',
        get_string('remotecohortstable', 'enrol_poasdatabase'),
        get_string('remotecohortstable_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcohortname',
        get_string('remotecohortnamefield', 'enrol_poasdatabase'), '', 'cohortname'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcohortdescription',
        get_string('remotecohortdescriptionfield', 'enrol_poasdatabase'), '', 'description'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcohortidnumber',
        get_string('remotecategoryidnumberfield', 'enrol_poasdatabase'), '', 'idnumber'));

    // Enable updating cohorts.
    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/enableupdatingcohorts',
        get_string('enableupdatingcohorts', 'enrol_poasdatabase'),
        get_string('enableupdatingcohorts_desc', 'enrol_poasdatabase'), 0));

    $options = array('idnumber' => 'idnumber', 'name' => 'name');
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/updatecohortsmappingfield',
        get_string('updatecohortsmappingfield', 'enrol_poasdatabase'),
        get_string('updatecohortsmappingfield_desc', 'enrol_poasdatabase'), 'idnumber', $options));

    $options = array(ENROL_EXT_REMOVED_KEEP    => get_string('option_cohortextremovedkeep', 'enrol_poasdatabase'),
                     ENROL_EXT_REMOVED_SUSPEND => get_string('option_cohortextremovedsuspend', 'enrol_poasdatabase'),
                     ENROL_EXT_REMOVED_UNENROL => get_string('option_cohortextremovedunenrol', 'enrol_poasdatabase')
    );
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/unenrolcohortaction', get_string('extremovedaction', 'enrol'),
        get_string('extremovedcohortaction_desc', 'enrol_poasdatabase'), ENROL_EXT_REMOVED_SUSPEND, $options));

    // Keep enrolled cohorts.
    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/keepenrolledcohorts',
        get_string('keepenrolledcohorts', 'enrol_poasdatabase'),
        get_string('keepenrolledcohorts_desc', 'enrol_poasdatabase'), 1));

    // Enable removing duplicate cohorts.
    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/enableremovingduplicatecohorts',
        get_string('enableremovingduplicatecohorts', 'enrol_poasdatabase'),
        get_string('enableremovingduplicatecohorts_desc', 'enrol_poasdatabase'), 0));



    $settings->add(new admin_setting_heading('enrol_poasdatabase_usercohortsheader',
        get_string('settingsheaderusercohort', 'enrol_poasdatabase'), ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/usercohorttable',
        get_string('remoteusercohorttable', 'enrol_poasdatabase'),
        get_string('remoteusercohorttable_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/cohortidnumber',
        get_string('remoteusercohortcohortidnumberfield', 'enrol_poasdatabase'), '', 'cohortidnumber'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/userlogin',
        get_string('remoteusercohortuserloginfield', 'enrol_poasdatabase'), '', 'userlogin'));
    $options = array(ENROL_EXT_REMOVED_KEEP    => get_string('option_usercohortextremovedkeep', 'enrol_poasdatabase'),
                     ENROL_EXT_REMOVED_UNENROL => get_string('option_usercohortextremovedunenrol', 'enrol_poasdatabase')
    );
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/unenrolusercohortaction',
        get_string('extuserremovedfromcohortaction', 'enrol_poasdatabase'),
        get_string('extuserremovedfromcohortaction_desc', 'enrol_poasdatabase'), ENROL_EXT_REMOVED_KEEP, $options));



    $settings->add(new admin_setting_heading('enrol_poasdatabase_newcoursesheader',
        get_string('settingsheadernewcourses', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcoursetable',
        get_string('newcoursetable', 'enrol_poasdatabase'),
        get_string('newcoursetable_desc', 'enrol_poasdatabase'), ''));


    $settings->add(new admin_setting_configtext('enrol_poasdatabase/tagstable',
        get_string('remotetagtable', 'enrol_poasdatabase'), '', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/sqlidnumberfield',
        get_string('remotetagcourseidnumberfield', 'enrol_poasdatabase'), '', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/sqltagnamefield',
        get_string('remotetagtagnamefield', 'enrol_poasdatabase'), '', ''));



    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcoursefullname',
        get_string('newcoursefullname', 'enrol_poasdatabase'), '', 'fullname'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcourseshortname',
        get_string('newcourseshortname', 'enrol_poasdatabase'), '', 'shortname'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcourseidnumber',
        get_string('newcourseidnumber', 'enrol_poasdatabase'), '', 'idnumber'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcoursecategory',
        get_string('newcoursecategory', 'enrol_poasdatabase'), '', ''));

    $settings->add(new admin_settings_coursecat_select('enrol_poasdatabase/defaultcategory',
        get_string('defaultcategory', 'enrol_poasdatabase'),
        get_string('defaultcategory_desc', 'enrol_poasdatabase'), 1));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/templatecourse',
        get_string('templatecourse', 'enrol_poasdatabase'),
        get_string('templatecourse_desc', 'enrol_poasdatabase'), ''));

    // Section.
    $settings->add(new admin_setting_heading('enrol_poasdatabase_coursescohortsheader',
        get_string('settingsheadercoursecohort', 'enrol_poasdatabase'), ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/groupcohortcoursestable',
        get_string('remotecoursecohorttable', 'enrol_poasdatabase'),
        get_string('remotecoursecohorttable_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/groupcohortidnumber',
        get_string('remoteusercohortcohortidnumberfield', 'enrol_poasdatabase'), '', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/courseidnumber',
        get_string('remotecourseidnumberfield', 'enrol_poasdatabase'), '', ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/groupname',
        get_string('remotegroupnamefield', 'enrol_poasdatabase'), '', ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remotecohortmemberrole',
        get_string('remotecohortmemberrole', 'enrol_poasdatabase'),
        get_string('remotecohortmemberrole_desc', 'enrol_poasdatabase'), 'member_role'));


    // Section.
    $settings->add(new admin_setting_heading('enrol_poasdatabase_laggingstudentsheader',
        get_string('settingsheaderlaggingstudents', 'enrol_poasdatabase'), ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/laggingstudentstable',
        get_string('laggingstudentstable', 'enrol_poasdatabase'),
        get_string('laggingstudentstable_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/laggingstudentlogin',
        get_string('laggingstudentlogin', 'enrol_poasdatabase'),
        get_string('laggingstudentlogin_desc', 'enrol_poasdatabase'), 'student_login'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/laggingexactcourseidnumber',
        get_string('laggingexactcourseidnumber', 'enrol_poasdatabase'),
        get_string('laggingexactcourseidnumber_desc', 'enrol_poasdatabase'), 'exact_course_idnumber'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/laggingcourseidnumberpattern',
        get_string('laggingcourseidnumberpattern', 'enrol_poasdatabase'),
        get_string('laggingcourseidnumberpattern_desc', 'enrol_poasdatabase'), 'course_idnumber_pattern'));

}
